Kõik andmed on ühe kihilised ning neil puuduvad igasugused parameetrid. Aluseks on võetud: Ruumiliselt ühtne linnaregioon.pdf

Keskused:
S02_keskus_tallinn-keskus-pt.geojson
S02_keskus_tallinn-keskus.geojson
S02_keskus_tallinn-keskus-pt.geojson
S02_keskus_harju-l2hikeskus-pt.geojson
S02_keskus_linnaline-ala.geojson

Alad:
S02_keskus_linnaline-ala.geojson
S02_keskus_mitmekeskused.geojson

Perspektiivne infra:
S02_keskus_perspektiivne-maatee.geojson
S02_keskus_perspektiivne-tramm.geojson
S02_keskus_perspektiivne-rong.geojson

Olemasolev infra:
S02_keskus_olemasolev-rong.geojson
S02_keskus_olemasolev-p6himaantee.geojson





