---
slug: annex-3-1
title: Read more 3.1
group: lisa
parent: healthy-mobility
background: "#f79da3"
---

<Hero>

<RunningHeader>READ MORE 3.1 – Healthy mobility</RunningHeader>

## Why this goal?

</Hero>

<Content>

The 'Healthy mobility' goal combines two important topics: health and physical activity. Health and/or healthcare and transport are usually addressed separately, but it was found during the preparation of the development strategy that health is so closely related to transport and physical activity from the viewpoint of a local government that they can be addressed together. The word play in the Estonian name of the goal ('Terve Tallinn liigub' – 'terve' means both 'healthy' and 'whole') also indicates that physical activity/mobility opportunities must be guaranteed to everyone for their daily routines as well as for recreation. Therefore, the principle of accessibility mentioned in the 'Friendly urban space' goal gets a separate subsection here in order to emphasise the importance of this topic.

Good health is not beneficial to people alone, but to the society as a whole. A healthy person has bigger capacity for work, and the expenses of treating their illnesses and paying them sickness and social benefits are smaller. The poor health of people restricts economic growth primarily for the following reasons: it increases the likelihood of early retirement and the state's expenses related to treating illnesses and rehabilitation, and it reduces participation in the labour market, the number of working hours and income. Thus, the length of a healthy life is of critical importance for increasing the employment of the elderly.

The goal 'Healthy mobility' is closely related to other goals as well. There is a strong synergic connection with the goal 'Friendly urban space' – a higher-quality public space promotes walking, cycling and using public transport, reduces forced mobility and makes being outdoors pleasant. The increased use of active types of mobility and public transport makes urban space friendlier, as there are more people and fewer cars on the streets. The achievement of the goal 'Creative global city' partially depends on how good the intra-city, regional and international connections are. The implementation of the goal 'Green transformation' largely depends on reducing the environmental impact of transport, and the achievement of the goal 'Home that includes the street' is affected by the reduction of aboveground parking spaces.

<!---
<ListHeader>
  UN sustainable development goals linked to this strategic goal
</ListHeader>

<ImageGrid maxWidth="88px">

![YRO](../meedia/yro/goal_eng_01.png)

![YRO](../meedia/yro/goal_eng_02.png)

![YRO](../meedia/yro/goal_eng_03.png)

![YRO](../meedia/yro/goal_eng_04.png)

![YRO](../meedia/yro/goal_eng_05.png)

![YRO](../meedia/yro/goal_eng_06.png)

![YRO](../meedia/yro/goal_eng_07.png)

![YRO](../meedia/yro/goal_eng_08.png)

![YRO](../meedia/yro/goal_eng_09.png)

![YRO](../meedia/yro/goal_eng_10.png)

![YRO](../meedia/yro/goal_eng_11.png)

![YRO](../meedia/yro/goal_eng_12.png)

![YRO](../meedia/yro/goal_eng_13.png)

![YRO](../meedia/yro/goal_eng_14.png)

![YRO](../meedia/yro/goal_eng_15.png)

![YRO](../meedia/yro/goal_eng_16.png)

![YRO](../meedia/yro/goal_eng_17.png)

![YRO](../meedia/yro/goal_eng_18.png)

</ImageGrid>
--->

## How do we assess our progress towards the goal?

The movement towards this goal is mainly assessed with health indicators, physical activity and mobility indicators.

**Health expectancy** is one of the three main indicators of the sustainable development goal 'Health and welfare' and the goal 'The people living in Estonia are smart, active and maintain their health' of Estonia 2035. In Estonia as a whole, health expectancy (at age 0) has indicated a certain trend of growth in recent years, increasing from 50 years (2005) to 54 years (2018) and reaching 57 years between (2016 and 2017). In Harju County, this indicator is permanently a little better: health expectancy has increased from 55.86 (2006) to 57.83 (2018) and was the highest (59.08) in 2007/2008. The indicator differs somewhat in the case of women and men: while the health expectancy of the women of Harju County has increased from 58.1 (2006) to 58.4 (2018) years, the same indicator in the case of men is 53.7 (2006) and 57.4 (2018) years. As these statistics are currently not published for Tallinn separately from Harju County, it is necessary to consider whether the collection of such statistics is reasonable or whether the health of the residents of Tallinn should be assessed according to the indicators of Harju County.

The health expectancy of people in European Union Member States has also increased from 61.8 (2010) to 64.0 (2018) on average. As we can see, Harju County and Estonia as a whole are considerably below the EU average, in fact ranking second from last after Latvia.[^1] Thus, we really have to work hard to achieve a significant increase in health expectancy.

The increase in health expectancy is the main indicator of the Public Health Development Plan 2020-2030 (PHDP). A joint seminar of the steering committee of the PHDP, the Research and Innovation Council and the local government level is held every year to present the results achieved and to identify and discuss the horizontal topics that must be solved. This means that Tallinn is one of the parties that contributes to the achievement of this objective and assesses said indicator primarily in the context of whether the indicators of the areas of activity of Tallinn, which should contribute to the increase in health expectancy, have improved. For example, whether the share of people engaged in physical activity has increased.

Health and physical activity are closely connected to each other. In addition to awareness, the daily types of mobility preferred by people also depend on how convenient one or another type is for them. This is assessed with objective indicators **(division of types of mobility)** as well as subjective ones **(share of people satisfied with mobility options)**. Annual satisfaction surveys identify the aspects of mobility possibilities that people are more or less satisfied with. The survey carried out in 2019 indicated that the respondents were the least satisfied with the possibilities of parking and storing bicycles close to home, and they were also concerned about car parks (adequacy of parking spaces and the procedure for parking close to home). The share of fully dissatisfied respondents was the highest in respect of the adequacy of cycling lanes and the maintenance of roads in city blocks during winter. It must be noted that the share of people who were very satisfied with the condition of all roads and traffic options was small, especially in relation to cycling and the mobility opportunities of people with special needs (see the figure below).

![Rahuolu teedega](../meedia/lisad/joonis-3_en.svg)

_Figure 1. Satisfaction with the condition of roads and traffic options in a person's city district (Satisfaction Survey of the Citizens of Tallinn 2019)._

In the comparison of the last five years, satisfaction with most aspects has remained the same or improved, and the opinions on the condition of roads and pavements in city blocks has improved the most. Opinions on a couple of aspects have deteriorated, e.g. people are less satisfied with the adequacy of cycling lanes and car parks and the opportunities for parking and storing bicycles close to their homes. The balance of the division of types of mobility has been set as a separate goal in the area of mobility: **The share of everyday journeys made by the residents of the Tallinn region by public transport, on foot or by bicycle is at least 50% by 2025 and 70% by 2035.** The increase in the share of active types of mobility is particularly important here, as it has the biggest positive impact on people's health. The **share of schoolchildren who can move independently** is measured separately. Traffic safety is also a direct connection between health and mobility. Thus, the indicators related to safety in the area of mobility are monitored under this goal **(road users consider the Tallinn mobility environment safe and the number of traffic accidents with human victims will decrease by half)**.

![Rahuolu teede olukorraga](../meedia/lisad/rahulolu-teede-olukorraga.png)

_Table 1. Satisfaction with the condition of the roads and traffic opportunities in one city district in comparison with other city districts (arithmetic averages on a four-point scale, where 1 = very dissatisfied and 4 = very satisfied) (Satisfaction Survey of the Citizens of Tallinn 2019)._

There are many circumstances that have an impact on the type of mobility people choose. Thus, other related indicators must be taken into consideration when moving towards the strategic goal. For example, people consider **how comprehensive the main and health network of cycle lanes is** (including the share of people to whom the main network of cycle lanes is accessible at up to 500 m), how well **the ten most important destinations in the city** can be reached by public transport, **the extent to which the existence of public transport stops within 400 m of most residents** is guaranteed and **the share of people who have unobstructed access to public transport stops.** Public transport must be fast in order to be the preferred option, which is why one of the indicators in the area of mobility that is also observed in the case of strategic goal in question is the duration of the public transport ride: **it should take no longer than half hour on average and, as a rule, no more than 20 minutes between the most important centres of Tallinn.** In the case of accessibility, the compliance of public transport vehicles and stops with the requirements of universal design and the decrease in the number of pedestrian crossings with high kerbstones are also observed. A detailed overview of accessibility is provided by the accessibility information system.

As mentioned in the case of the indicator of health expectancy, the indicators set in various areas are also observed to assess progress towards the goal. For example, the increase in health expectancy is supported by regular physical activity. Physical activity has a direct impact on the mental and physical health of people and thereby on health expectancy. To this end, it is observed how **the regular physical activity of Tallinn residents (aged 15-74) changes (the objective: people are consciously physically active at least twice a week for 30 minutes) and the share of young people (aged 7-19) who are moderately or intensively physically active for at least 60 minutes every day.**

In order to increase health expectancy, it is important that people are health-conscious and follow a more balanced diet and that risky behaviour decreases. It is observed how **inequality in health** changes (assessment of the National Institute for Health Development) and all **indicators of the action programmes in the health sector** are constantly monitored. Higher **satisfaction with the accessibility of health services** and a bigger **share of students covered by health checks** also support a longer health expectancy.

Also, similarly to the goal 'Friendly urban space', the achievement of the goal 'Healthy mobility' requires **the city to have the necessary real estate**, e.g. for the development of infrastructure. The achievement of the target levels of this indicator is therefore also monitored.

## City's areas of activity that support the achievement of this goal

As the goal 'Healthy mobility' addresses the health and physical activity of people, the areas that contribute the most to its achievement are **health and healthcare, mobility, physical activity and sports.** The areas of **urban planning** and **city property** also play a very important role in the achievement of the strategic goal by supporting the achievement of an urban space that supports health and physical activity and is accessible. All action programmes in the **area of health and healthcare** contribute to making the residents of Tallinn healthier and live longer and minimising the risks arising from the living environment and the health behaviour of people. Above all, efforts are made to increase health awareness and prevent risk behaviour by promoting healthier choices and lifestyles. Optimal physical activity, a balanced diet, safe road use and leisure activities, risk-free sexual behaviour and lack of addictive habits help prevent many diseases and allow people to be as healthy as possible for their entire lives. Separate attention is given to guaranteeing the health and safe development of children and young people, as the experience gained in childhood has an impact on an adult's values, social coping skills and health behaviour. A working, learning and living environment that supports health as well as accessible and contemporary health services also contribute to the longer health expectancy of the residents of Tallinn.

Active types of mobility have a significant impact on people's health. Similarly to goal 'Friendly Urban Space', the **area of mobility** also supports the achievement of the goal 'Healthy mobility' by trying to improve the balance of mobility in Tallinn. To this end, it is necessary to increase the share of everyday use of public transport, walking or cycling. Fast, convenient and pleasant public transport connections between the centres of the city and with neighbouring municipalities create the prerequisite for people preferring public transport instead of their own cars and combining different types of mobility. This in turn helps reduce the quantity of cars in urban space, make car traffic smoother and reduce traffic jams. The area also covers the implementation of the principles of universal design in Tallinn, which allows everyone to move independently in the city. The streets, pavements, public transport stops and the main network of cycle lanes of Tallinn must be accessible to everyone all year round. It is very important to pay attention to guaranteeing safety so that children and the elderly can also move independently. This calls for the use of a comprehensively designed urban space, streets that are in good order and maintained well all year round, use of traffic calming measures and speed limits as well as all road users being considerate of one another. The **area of physical activity** supports an active and athletic lifestyle through awareness raising, development of sports infrastructure and services and benefits. The goal is to ensure that physical activity is a natural part of every citizen's life and is supported by diverse opportunities both close to home as well in citywide sports facilities located further away.

**Urban planning** contributes to the 'Healthy mobility' goal by creating a friendly urban space. As noted above, the achievement of these two objectives is closely related. In addition, the plans provide for specific attractive places for outdoor activities that inspire people to spend time outdoors irrespective of the weather. Reducing the flood risk and the impact of heat islands is also very important. When developing the accessibility policy, the city makes sure that urban space is made accessible to everyone.

**The preservation and development of city property supports** the achievement of the goal primarily by ensuring that the city has enough land or rights for the development of mobility services at a new level. The urban environment will also be made accessible by implementing the principles of universal design in designing the real estate environment. This is achieved through the use of planning and land readjustment and participation in the development of the related policies, principles and guidelines. Ensuring a living environment that supports health is supported through the action programme of buildings that use innovative solutions, which helps to guarantee a good indoor climate and the standard CO₂ level in the renovated city buildings (including health, welfare and educational institutions).

In addition to the aforementioned three fields, there are aspects that contribute to the achievement of the goal 'Healthy mobility' in the following fields as well.

**Social welfare** – This supports the independent coping of citizens by providing needs-based support for the prevention of poverty risk and supporting people who are already deprived. Social welfare thereby contributes to people being mentally and physically healthier and living longer.

**Urban landscape** – An attractive urban space promotes spending time outdoors and safe movement in recreational areas, such as parks, playgrounds, skating rinks and dog walking parks. The area contributes to the achievement of the goal 'Healthy mobility' by ensuring that the green areas of the city are accessible and attractive, inviting people to use them for physical activities and ensuring that their functions are based on the needs of citizens. With the planning of a network of playgrounds and the construction and maintenance of playgrounds, the area contributes to children having more opportunities and reasons for being outdoors, which in turn supports their health. The maintenance of dog walking parks also helps to ensure that people feel good when being outdoors with their pets. The area also contributes to the achievement of the goal with other seasonal or one-off projects (e.g. outdoor toilets, drinking places and ice rinks) that all support spending time and being active outdoors.

**Environmental protection** – Similarly to urban landscape, the area helps to create an urban space that invites people to go outdoors and be active irrespective of the weather. For example, tall vegetation offers shade during hot summers or rainy autumns. The area also deals with topics that have a direct impact on health – clean water and air and less noise.

**Utility networks** – Lighting that makes urban space usable during the hours of darkness is very important for everyday mobility and physical activity.

[^1]: Eurostat. https://ec.europa.eu/eurostat/databrowser/view/tps00150/default/bar?lang=en

</Content>
