---
slug: annex-6-1
title: Read more 6.1
group: lisa
parent: home-that-includes-the-street
background: "#d6e9f8"
---

<Hero>

<RunningHeader>READ MORE 6.1 – Home that includes the street</RunningHeader>

## Why this goal?

</Hero>

<Content>

The strategic goal 'Home that includes the street' covers everything related to housing, such as their diversity, connection to urban space, accessibility and availability so that every household in the city can find a home that corresponds to its size and other needs.

The topic of home has been moved to a higher level in comparison with the effective development strategy. There are two main reasons for this. Firstly, a person's home forms a major part of the experience of living in a city. This concerns all people and largely determines their quality of life. Secondly, the global trend is that the accessibility of homes decreases as urbanisation grows and jobs with higher added value are created. In July 2018, Amsterdam, Barcelona, London, Montréal and Montevideo submitted a joint declaration[^1] to the United Nations, where they pointed out that people's right to accessible housing is at risk. Goal 11 of the UN Sustainable Development Goals also states that everyone must be guaranteed adequate, safe and affordable housing and utility services. The situation concerning the accessibility of housing is currently rather good in Tallinn. The goal set with the strategy is to increase the competitiveness and international attractiveness of Tallinn. As this had led to a decrease in the accessibility of housing in other cities, there is reason to assume that this will occur in some form in Tallinn as well. This is also influenced by the fact that the population of Tallinn will presumably increase and we have a lot of residential buildings built in the seventies and eighties of the previous century, which are becoming outdated at the same time.

A home has been regarded as something broader than a place of residence in the context of the goal. The definition of a home has been extended to the neighbourhood and the community. The goal 'Home that includes the street' is strongly connected to the goal 'Friendly urban space'. These two goals have an impact on one another. On the one hand, the location and type of residential buildings has an impact on the kind of space we create: are we creating a friendly urban space? On the other hand, the attractiveness of homes depends on the surrounding environment and the services provided there. This mutual connection is primarily highlighted in the first two sections 'Multi-purpose residential areas' and 'Unique subdistricts and yards'.

The third section discusses that there should be enough homes (the supply covers the demand) and that they are of suitable size, affordable and accessible. Size is addressed because the number of square metres per resident here is smaller than the European average, affordability because it may deteriorate due to the improvement of the standard of living (at least for certain social groups) and accessibility because the majority of the existing housing fund is not accessible to everyone.

The fourth topic arises directly from the goal of green transformation. As the activities related to the use of buildings are one of the three biggest sources of CO₂, the achievement of the goal of green transformation depends partly on the sustainability of buildings. In the long run, this is also an issue of accessibility – fixed costs are lower in more energy-efficient buildings. Comfort also covers the healthy indoor climate. However, the action taken to reduce energy costs may not worsen the indoor climate of the buildings, which has happened before, e.g. as a result of poor renovation.

<!---
<ListHeader>
  UN sustainable development goals linked to this strategic goal
</ListHeader>

<ImageGrid maxWidth="88px">

![YRO](../meedia/yro/goal_eng_01.png)

![YRO](../meedia/yro/goal_eng_02.png)

![YRO](../meedia/yro/goal_eng_03.png)

![YRO](../meedia/yro/goal_eng_04.png)

![YRO](../meedia/yro/goal_eng_05.png)

![YRO](../meedia/yro/goal_eng_06.png)

![YRO](../meedia/yro/goal_eng_07.png)

![YRO](../meedia/yro/goal_eng_08.png)

![YRO](../meedia/yro/goal_eng_09.png)

![YRO](../meedia/yro/goal_eng_10.png)

![YRO](../meedia/yro/goal_eng_11.png)

![YRO](../meedia/yro/goal_eng_12.png)

![YRO](../meedia/yro/goal_eng_13.png)

![YRO](../meedia/yro/goal_eng_14.png)

![YRO](../meedia/yro/goal_eng_15.png)

![YRO](../meedia/yro/goal_eng_16.png)

![YRO](../meedia/yro/goal_eng_17.png)

![YRO](../meedia/yro/goal_eng_18.png)

</ImageGrid>
--->

## How do we assess our progress towards the goal?

Progress towards this strategic goal is assessed based on two main indicators, i.e. **the share of people who are very satisfied with their home and the share of housing costs in total costs.**

Satisfaction with housing is measured with the annual satisfaction survey. Satisfaction with current housing is surveyed from four aspects: satisfaction with the location, condition, comfort and size of the housing. According to the survey carried out in 2019, approximately half of the respondents were fully satisfied with most of the aspects. The largest number of respondents were very satisfied with the location of their housing (ca two-thirds of the respondents, see the figure below)[^2].

![Rahulolu eluruumiga](../meedia/lisad/joonis-6_en.svg)

_Figure 1. Satisfaction with current housing (Satisfaction Survey of the Citizens of Tallinn 2019)._

As the total share of people who are very satisfied and somewhat satisfied is very high, the increase in the share of residents who are very satisfied is observed in the context of achievement of the goal while keeping in mind that the aforementioned total share should not decrease.

In the comparison of districts, the satisfaction of residents with the location of their housing is the highest in Northern Tallinn, City Centre and Pirita and the lowest in Lasnamäe. Opinions of the comfort of housing are better than the average in Pirita and Haabersti/Õismäe, and opinions on the condition of housing are the highest in Pirita and the City Centre. On average, the residents of Pirita and Haabersti/Õismäe are the most satisfied with their housing and the residents of Northern Tallinn are the most dissatisfied.

As for the type of building and preferences, the satisfaction survey of 2019 indicates that 87% of citizens live in apartment buildings, 10% in detached houses and 3% in semi-detached or terraced houses, but only 41% of respondents would prefer to live in apartment buildings while 43% prefer detached houses and 12% semi-detached or terraced houses[^3].

![Hoonetüübid](../meedia/lisad/joonis-7_en.svg)

_Figure 2. Type of existing housing and type of housing preferred (%) by citizens (Satisfaction Survey of the Citizens of Tallinn 2019)._

**Share of housing costs in total costs** is also an important indicator of the strategic goal, which shows the accessibility of homes to residents and the potential for a person to swap their current home for a more suitable one. According to the data of the Estonian Household Budget Survey (2019), housing costs are the biggest cost article with _ca_ 15% alongside food and non-alcoholic beverages (15-28%). They are followed by transport and leisure expenses. Total consumption expenses comprise ca 98.5% of all expenses (total households). The share of housing expenses has remained the same in the last three years but has decreased over a period of ten years.

The extent to which the size of housing corresponds to the needs of the household illustrates the accessibility of housing. **The change in the number of rooms per household member** is therefore monitored. A positive change in the last 10-15 years is that the share of households that have more than one room per household member has increased consistently (from 30% in 2005 to 46% in 2016). The share of households with one room per household member has decreased by 7% in the same period (40% vs 33%) and the share of households that have fewer than one room per household member has decreased by ca 9% (30% vs 21%). From the viewpoint of reducing costs, it is also important that homes are energy-efficient. It is therefore observed how the **share of energy-efficient buildings** is changing in the city (the share of buildings of energy class A and B should increase).

Increasing the share of lower residential buildings (see the section 'Unique subdistricts and yards') is an important part of the 'Home that includes the street' goal. Therefore, the percentage of new or reconstructed homes in buildings of up to four storeys is monitored.

![Uued eluruumid](../meedia/lisad/joonis-8_en.svg)

_Figure 3. The number of new homes built from 2010 to 2019 according to the number of storeys of the building (EHR)._

The indicators set in the contributing areas are also used to assess the progress towards the strategic goal. The condition of the surrounding environment has an impact on satisfaction with housing. Therefore, it is important to monitor the indicators of **environmental protection action plans.** For example, if noise near home increases or the quality of air deteriorates, it affects people's satisfaction with the location of their housing. The value of a home is increased by the aspects of maintenance and greenery – are the outdoor areas clean **(the satisfaction of citizens with the cleanliness of urban space** is monitored) and is there greenery nearby **(the share of green journeys; the share of residents for whom a green area is up to 300 m away)?** The value of a home is also increased by a good public transport and active mobility connections, which in turn is reflected in the indicators of the area of mobility, e.g. **the share of everyday mobility covered by physical activity, walking or cycling and the share of schoolchildren who can move independently.**

Providing the necessary housing services to people in need is also a duty of the city. The success of this is evident in **how the waiting list of people applying for municipal housing changes.**

## City's areas of activity that support the achievement of this goal

The achievement of the strategic goal is supported directly and mostly by the area of urban planning, preservation and development of urban property, mobility and utility networks.

**Urban planning** – The area creates the framework for the establishment of multi-purpose residential areas and unique subdistricts and yards. The framework means plans, guidelines, requirements, etc., which are used to direct residential development mainly to the vicinity of centres along with public transport and everyday services, make yards look unique and reduce the share of aboveground parking spaces in yards. The activities of the area support the planning of homes of various sizes so that the needs of different households are met and the majority of residential buildings are located in up to four-storey buildings. Several types of new homes suitable for all types of households are built in city districts.

**Preservation and development of city assets** – The area mainly contributes to the goal by organising the construction of new residential buildings and the reconstruction of existing ones. Its goal is to ensure need-based, affordable, accessible, energy-efficient and comfortable homes in different subdistricts also for people in need of help and for workers who are important to the city (e.g. teachers, medical professionals). Social housing units and municipal housing are planned everywhere in the city proceeding from a comprehensive living environment. The area also contributes to making homes in Tallinn more energy-efficient. The area improves the capacity of apartment associations to coordinate their activities and distributes financial support for the organisation of yard areas.

**Mobility** – Whether or not a home starts farther than a person's front door largely depends on what the environment surrounding the home is like. This in turn depends significantly on the organisation of mobility, particularly the number of parking spaces, the speed of traffic and the street space allocated to pedestrians and cyclists. Good public transport and light traffic connections also increase the value of homes. Promoting active, economical and environmentally friendly types of mobility will make the city air cleaner and reduce noise, which is essential for creating a healthy and attractive living environment.

**Utility networks** – The area helps organise the utility services provided by the private sector and ensures street lighting, which makes residential areas more attractive and safe.

In addition to the aforementioned areas, other aspects that contribute to the achievement of the strategic goal 'Home that includes the street' can be mentioned in respect of the following areas.

**Social welfare** – The area contributes to the strategic goal by supporting the adaptation of housing. This helps make homes accessible to people with special needs and the elderly, including adjusting the rooms according to their needs. The area supports the goal of accessibility of urban space by developing the accessibility policy and supervising the implementation of accessibility principles.

**Urban landscape** – The area helps make subdistricts and yards more unique, cleaner and safer. Outside areas are greener because of vegetation and there are more reasons to spend leisure time there. The area also plans a network of playgrounds and builds and maintains playgrounds. This gives children and families more opportunities and reasons to spend time in their neighbourhoods.

**Environmental protection** – The area of environmental protection has a major impact on the fact that the sense of home is not limited to four walls but also covers a person's home street or the entire subdistrict. Two action programmes – 'Clean air' and 'Less noise' – contribute to the strategic goal above all by making streets and neighbourhoods more attractive.

**Business environment** – The contribution of the area to the strategic goal is indirect but still important. More competitive companies help increase the standard of living in the society, which in turn increases the accessibility of suitable homes. However, the increase in the standard of living must be accompanied by an increase in the supply of housing, otherwise housing will become even less accessible to people with lower income. A problem that concerns many cities that are attractive tourist destinations is that increasingly more tourists are using rental apartments for accommodation instead of hotels. This has reduced the accessibility of housing, which is why this trend must also be monitored in Tallinn.

[^1]: See https://www.uclg.org/sites/default/files/cities_por_adequate_housing.pdf
[^2]: Satisfaction Survey of the Citizens of Tallinn 2019-3.
[^3]: Satisfaction Survey of the Citizens of Tallinn 2019-3.

</Content>
