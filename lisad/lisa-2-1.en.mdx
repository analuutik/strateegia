---
slug: annex-2-1
title: Read more 2.1
group: lisa
parent: creative-global-city
background: "#adadad"
---

<Hero>

<RunningHeader>READ MORE 2.1 – Creative global city</RunningHeader>

## Why this goal?

</Hero>

<Content>

The goals set in the Development Strategy Tallinn 2035 can only be achieved if social prosperity, knowledge and creativity (innovation) increase in all areas. In a situation where cities compete for investments (related to prosperity) as well as for top specialists (related to knowledge and creativity), Tallinn must be attractive as a business and living environment to us as well as to foreign entrepreneurs and workers. If we are able to develop and keep and attract specialists with the necessary skills, investments will also come to Tallinn. Money looks for skilled labour in the 21st century.

Designing all kinds of environments is important: a favourable tax environment, nationwide lifelong learning as well as bicycle lanes. Although these aspects don't depend on Tallinn alone, people who can choose assess the desirability of Tallinn as a living and working environment according to these and other areas. The strategic goal 'Creative global city' is dedicated to the establishment of an environment that is suitable for the development of skills and collaboration (innovation) between various people.

In order to win in the competition of cities, Tallinn must not only be a city with a better living environment and services, but also have a story to tell the world. The story told to the world by the strategy Tallinn 2035 is that of a dynamic twin city, the heart of smart economy – it is a meeting place of educated people, technology, human-centred development and high-level design.

Of the UN Sustainable Development Goals[^1], the goal „Creative global city„ contributes mostly to economic subsistence, food security, quality education, employment and economic growth, industry, innovation and infrastructure, sustainable cities and subdistricts and cultural viability.

<!---
<ListHeader>
  UN sustainable development goals linked to this strategic goal
</ListHeader>

<ImageGrid maxWidth="88px">

![YRO](../meedia/yro/goal_eng_01.png)

![YRO](../meedia/yro/goal_eng_02.png)

![YRO](../meedia/yro/goal_eng_03.png)

![YRO](../meedia/yro/goal_eng_04.png)

![YRO](../meedia/yro/goal_eng_05.png)

![YRO](../meedia/yro/goal_eng_06.png)

![YRO](../meedia/yro/goal_eng_07.png)

![YRO](../meedia/yro/goal_eng_08.png)

![YRO](../meedia/yro/goal_eng_09.png)

![YRO](../meedia/yro/goal_eng_10.png)

![YRO](../meedia/yro/goal_eng_11.png)

![YRO](../meedia/yro/goal_eng_12.png)

![YRO](../meedia/yro/goal_eng_13.png)

![YRO](../meedia/yro/goal_eng_14.png)

![YRO](../meedia/yro/goal_eng_15.png)

![YRO](../meedia/yro/goal_eng_16.png)

![YRO](../meedia/yro/goal_eng_17.png)

![YRO](../meedia/yro/goal_eng_18.png)

</ImageGrid>
--->

## How do we assess our progress towards this goal?

The progress towards the goal is assessed on the basis of two main indicators: rate of participation in lifelong learning and productivity per employed person (on the basis of added value). **The share of participants in lifelong learning among people aged 25-64** is also an indicator of sustainable development. According to Statistics Estonia, the rate of participation in lifelong learning (measured in the last four weeks) has increased from 10.5% (2009) to 19.7% (2018) in Estonia and from 15.8% to 24.5% in Tallinn in the last ten years. As lifelong learning is essential for responding flexibly to the rapid changes in society, the goal we have established is to significantly increase participation in lifelong learning. According to Eurostat[^2], Estonia with its rate of ca 20% ranked fourth in the European Union in 2019 after Sweden (34.1%), Finland (28.6%) and Denmark (24.8%). This is an excellent result and underlines the good level of the Estonian education system but considering the fact that smart and creative people are basically our only competitive advantage, our ambition in lifelong learning could also be to reach the top of the world, similar to the PISA tests.

The rate of participation in lifelong learning can be increased if our learners want to learn, they benefit from it to the maximum and they feel that they have sufficient support if they need it. Achievement of the goals to be set in the field of education (subjective wellbeing of learners and teachers, satisfaction with the learning environment) is therefore monitored within the scope of the goal.

Increasing the productivity of companies is one of the main tasks in promoting competitiveness, especially in a situation where the production costs of companies are increasing. The productivity of companies has increased year on year alongside the increase in wages and people's income, in both Tallinn and Estonia.

A number of statistical indicators are used to describe the productivity of companies. The development strategy of Tallinn uses **productivity**[^3] **per employed person based on added value**[^4] to measure the strategic goal. Productivity per employed person has increased in the last decade, rising from 27,400 euros (2012) to 32,500 euros (2017) in Tallinn and thereby exceeding the Estonian average (31,000 euros, 2017). **Productivity per employed person based on sales revenue**[^5] in 2017 was 144,000 euros in Tallinn and 134,000 euros on average in Estonia. In addition, hourly productivity indicators of companies may be used to analyse the situation. In 2017, there were 3598 companies in Estonia, including 1644 in Tallinn, where the annual average number of employees was 264,507 and 134,622, respectively. The hourly productivity of companies with 20 and more employees on the basis of **hourly productivity based on sales revenue**[^7] in 2017 was 78 euros on average in Estonia and 84 euros in Tallinn. The average hourly productivity calculated based on **added value**[^8] in 2017 was 18 euros in Estonia and 19 euros in Tallinn. It is appropriate to note that the productivity indicators of the companies located in Tallinn are traditionally somewhat better than the average in Estonia.

Although the **productivity of Estonian companies per employed person in respect to the EU average** has increased by more than 10% over the last decade (from 67.2% in 2009 to 77.9% in 2018), Estonia ranks in the last third among EU Member States with this indicator. Thus, the development potential here is significant.

The indicators set in the contributing areas are also used to assess the progress towards the strategic goal. For example, productivity can improve when companies increase investments in people and R\`&D. It is therefore observed how **the investments of companies in property, plant and equipment and in intangible assets as well as the share of investments in GDP change** (the EU average is 37%). The desire and opportunities of people to engage in entrepreneurship is reflected in the number of new businesses established every year (**number of companies established, growth compared with the previous year**) and in the number of active companies (number of companies per 1000 residents), including start-ups that are still developing their products or services (**number of start-up companies per 100,000 residents**). The companies' ambition and ability to succeed are characterised by the share of export in their turnover and the share of foreign investments in the volume of investments. Enterprise Estonia found in the survey on the export potential of Estonia that „the Estonian export portfolio as a whole has the potential to increase its share on the current target markets, but this calls for success among tough competition“.[^9]

Rating tables are used for positioning cities on the international scale. Tallinn monitors its position in rating tables such as **European Digital City Index, CityKeys, Expat City Ranking**, as it strives to be in at least the top twenty. The opportunities offered by the city and its attractiveness are also reflected by how long visitors stay and how much they spend here **(the number of overnight stays by foreign guests in accommodation establishments; income per foreign guest by target group** – the indicator still needs to be developed) and how satisfied they are with the visiting experience **(satisfaction of foreign guests)**.

## City's areas of activity that support the achievement of this goal

Since the goal 'Creative global city' addresses, among others, the competitiveness of the economy of Tallinn, the achievement of this goal is indirectly supported by all areas of activity. However, we can highlight that **education and youth work, business environment and culture** are the areas whose contribution is the most direct and important.

The success of the economy of Tallinn depends almost entirely on people – their knowledge, skills, creativity and initiative – and the implementation of new technology. When we look at the increase in added value, we see that it is driven by knowledge-intensive companies. New knowledge and skills are also necessary in the sectors that may not usually be regarded as knowledge-intensive. The introduction of new technology, digitisation and automation will also contribute to the growth of added value in more classical sectors. This means that education, including youth work and culture, which deal with lifelong learning, are of utmost importance.

**Education and youth work.** Formal education creates the foundation for lifelong learning. The programme of an individual learning path is thereby particularly important, as it should bring out each student's talents and motivate them to develop themselves as much as possible. The city's diverse and integrated education network, which is supported by state upper secondary schools and other learning opportunities offered by the state and the private sector, contributes to learning being lifelong and visible in the physical as well as virtual space of the city. Opportunities for acquiring basic and general education in English support foreigners who have arrived here. Youth work contributes to the promotion of entrepreneurship and the development of creativity, supporting and encouraging young people to actively participate in social life and decision-making processes by offering them diverse opportunities for self-realisation and opportunities for unleashing their creative and development potential. For example, the entrepreneurship and initiative of young people are supported and volunteering and participation in youth associations and youth councils is encouraged. Youth work is especially important in order to notice those who potentially cannot keep up so that they can be offered support in the acquisition of formal education at the right time.

**Culture** is the area responsible for Tallinn having libraries and museums that allow for diverse and contemporary self-development and for developing hobby opportunities. The emphasis is on the word 'contemporary' because people's expectations of such services are constantly increasing and they compete with many leisure opportunities that are easy to consume.

**Business environment** as an area contributes to increasing interest in and awareness of business as well as shaping the image of an open city with innovation and a knowledge-intensive economy. The development of entrepreneurship is supported from adolescence. Youth work programmes and hobby activities create good opportunities for this. There are currently more than 350 student companies in Estonia, one-fifth of them in Tallinn. However, there is plenty of room for development in this area. The most direct contribution to the development of the business environment is made through industrial parks, business parks and incubators. To this end, Tallinn develops business incubators, associations of business and industrial parks and Tallinn Science Park Tehnopol as the testing environment of a smart city and organises business development projects that support entrepreneurship, are profitable and create added value for the city (e.g. Tallinn Film Wonderland). The city cooperates with stakeholders to turn the city into a favourable testing environment for new products and services and to increase the efficiency of international cooperation, including between Tallinn and Helsinki. The area also contributes to the establishment of an environment open to internationalisation, e.g. guaranteeing the accessibility of necessary services to foreign specialists and new immigrants, and the establishment of connections (e.g. Rail Baltic terminal, tram connection to city gates).

Tallinn also regards tourism as part of the area of activity of business environment. Tourism plays an important role in the overall economic development of Estonia, as the events on the tourism market are closely related to other economic sectors – tourism has an increasingly larger impact on all economic indicators of Estonia, including export and investments, employment and regional development. Income from international tourism in Estonia is remarkable in comparison with other European countries – _ca_ 1400 dollars per resident according to the UN World Tourism Organisation (UNWTO). This means that Estonia and Sweden are the leaders among Nordic and Baltic countries, and we exceed the levels of many European countries, including France and Italy. The share of tourism economy in the GDP and employment rate of Estonia with its indirect impact considered is ca 8% and tourism gives a large share of the export revenue of Estonia. The establishment of a multi-purpose conference centre and the stimulation of conference tourism as well as introducing Tallinn as a great destination for city breaks all year round and offering a memorable and comfortable city visit make it possible to raise international awareness of Tallinn even further, attract more tourists and top specialists, as well as increase the circle of creative people and exchange know-how.

The business environment in itself is a much broader concept than what is covered by the city under this area of activity. The business environment as a whole depends on the other areas of activity of the city and to a very large extent the policies of the state. Urban planning and mobility are the main areas that help make the business environment more attractive in a broader sense.

**Urban planning** creates a framework for the development of an attractive urban space. A war for talent is ongoing in today's economy. In the past, people moved to the places where companies were based; today, increasingly more companies (especially knowledge-intensive ones) move to the places where people live. And people in turn prefer regions that offer a pleasant physical environment. The area of urban planning is the one that designs the framework required for the creation of interesting urban space. Above all, this framework holds plans that have been prepared in a contemporary manner, balance public and private interest and guarantee the city's long-term interest. They are also the guidelines that address spatial development, proposals concerning the development of urban space, architecture competitions, analyses, surveys, etc. As the different historical levels of the urban space are one of the aspects of the attraction of Tallinn, the heritage conservation activities in the area of urban planning contribute to the achievement of the goal 'Creative global city'. Valuing the Old Town, which is on the UNESCO World Heritage List, or the areas of cultural and environmental value is therefore not merely an issue of cultural heritage, but also an aspect of competitiveness.

**Mobility** is one of the pillars of a good and well-functioning living environment as well as competitiveness – all mobility action programmes are important for the achievement of the goal 'Creative global city'. The importance of regional and international connections must be separately emphasised in this context. Fast, environmentally friendly, convenient and accessible connections with the neighbouring municipalities and all of Estonia as well as with Finland and other economic partners important to Estonia are an essential prerequisite for the development of connected capital regions. Although international connections are developed by private companies and infrastructure is mostly developed by the state, Tallinn can contribute to the improvement of the gates of these connections – the harbour and the airport – by extending the tram route to the harbour or contributing to the establishment of the Rail Baltic terminal via the planning process. In the context of regional connections, it is important to plan an integral and safe urban space, which takes into consideration various types of mobility and the locations of places of homes, schools, jobs and catchment centres. The establishment of convenient and fast public transport connections and the development of a common route network of the capital region, the main network of bicycle lanes and the health network and the provision of new mobility services are also important. In international context, it is necessary to develop smart transit routes and cooperation between Helsinki and Tallinn in order to direct the cargo traffic from the ports out of the city.

In addition to the aforementioned areas, the aspects highlighted below can be mentioned in relation to other areas that contribute to the achievement of the goal 'Creative global city'.

**Physical activity** – In the context of the goal 'Creative global city', this area contributes to Tallinn being a city of events that bring people together. Supporting competitive sports and sporting events and guaranteeing the accessibility of the necessary infrastructure means that various international and nationwide (major) sporting events are constantly held in the city, which can be enjoyed by citizens and visitors alike and which make Tallinn attractive to everyone.

**Environmental protection** – Diverse urban nature and a clean environment attract tourists, workers and investors to Tallinn. All action programmes in the area contribute to this – diverse and biodiverse urban nature, clean water and air, less noise and environmentally aware citizens.

**Health and healthcare** – A healthier environment for working, learning and living and more health-conscious choices help people to be healthy, happy and creative. This in turn helps increase employment and create more added value. A healthy working, learning and living environment as well as accessible and contemporary health services are an important reason for preferring Tallinn as a place of residence.

**Municipal order** – The safe and clean environment makes the city attractive in the eyes of both locals and people staying here temporarily. In the field of law enforcement, the Municipal Police helps ensure safety and order in the city with its presence, prevention and surveillance.

[^1]: UN Sustainable Development Goals: https://www.stat.ee/et/avasta-statistikat/valdkonnad/saastev-areng
[^2]: https://appsso.eurostat.ec.europa.eu/nui/show.do?dataset=trng_lfse_01&lang=en
[^3]: Productivity = added value / number of employed persons.
[^4]: Sales revenue + other operating revenue (excl. profit from sales and revaluation of fixed assets, revenue from government financing of fixed assets) – total costs – other operating expenses (excl. loss from sales and revaluation of fixed assets) + labour expenses + depreciation + change in inventories of goods in progress and finished goods (difference between the end and the beginning of the financial year) + fixed assets made for own use.
[^5]: Productivity = (sales revenue + income from government financing of operating expenses) / number of employed people.
[^6]: Productivity = (sales revenue + revenue from government financing of operating expenses) / number of hours worked.
[^7]: Productivity = added value / number of hours worked.
[^8]: Eurostat, https://ec.europa.eu/eurostat/tgm/table.do?tab=table&init=1&plugin=1&language=en&pcode=tec00116
[^9]: https://www.eas.ee/wp-content/uploads/2018/09/Eesti-ekspordipotentsiaal-2018.pdf

</Content>
