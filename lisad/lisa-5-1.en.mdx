---
slug: annex-5-1
title: Read more 5.1
group: lisa
parent: kind-community
background: "#f5d965"
---

<Hero>

<RunningHeader>READ MORE 5.1 – Kind community</RunningHeader>

## Why this goal?

</Hero>

<Content>

The main emphasis of the development strategy is understandably on spatial development and mobility. These are the topics that are the duties of local authorities pursuant to law and where citizens expect the most from the city. However, it must be kept in mind that in addition to being a physical entity, the city is also a social ecosystem and interaction as well as socio-economic coping can be influenced to a certain extent with the way the urban space is planned and public services are provided. The achievement of the goal of a kind community is largely related to the achievement of other goals (e.g. an urban space that fosters meetings and activities, the creation of an inclusive education system or good jobs), but there are also completely separate activities that stem mainly from this goal. The main aspects that help define the goal are highlighted in the summary of the goal. The sentence 'Social groups living together in neighbourhoods' refers to the creation of spatial cohesion between social groups. Security is addressed in the context of the improvement of the subjective sense of security as well as the decrease in objectively assessed accidents and crime. Helping people in need holds an important place in the case of this goal – contributing to fully independent coping, enabling partially independent coping or, if necessary, provision of permanent help. Civil society is also regarded in the light of kindness – caring about the city, the community, fellow citizens and shared values. This is certainly a topic where a relatively young democracy has room for development.

Among the UN Sustainable Development Goals, the goal 'Kind community' primarily contributes to there being no poverty (number 1), increasing gender equality (number 5), reducing inequalities (number 10) and inclusive institutions (number 16). In terms of the goals of Estonia 2035, it is directly related to an open, caring and cooperative society. As a small difference, the topics concerning culture have been defined in the Tallinn Development Strategy within the scope of the goal 'Creative global city'.

<!---
<ListHeader>
  UN sustainable development goals linked to this strategic goal
</ListHeader>

<ImageGrid maxWidth="88px">

![YRO](../meedia/yro/goal_eng_01.png)

![YRO](../meedia/yro/goal_eng_02.png)

![YRO](../meedia/yro/goal_eng_03.png)

![YRO](../meedia/yro/goal_eng_04.png)

![YRO](../meedia/yro/goal_eng_05.png)

![YRO](../meedia/yro/goal_eng_06.png)

![YRO](../meedia/yro/goal_eng_07.png)

![YRO](../meedia/yro/goal_eng_08.png)

![YRO](../meedia/yro/goal_eng_09.png)

![YRO](../meedia/yro/goal_eng_10.png)

![YRO](../meedia/yro/goal_eng_11.png)

![YRO](../meedia/yro/goal_eng_12.png)

![YRO](../meedia/yro/goal_eng_13.png)

![YRO](../meedia/yro/goal_eng_14.png)

![YRO](../meedia/yro/goal_eng_15.png)

![YRO](../meedia/yro/goal_eng_16.png)

![YRO](../meedia/yro/goal_eng_17.png)

![YRO](../meedia/yro/goal_eng_18.png)

</ImageGrid>
--->

## How do we assess our progress towards the goal?

Progress towards the goal is assessed by two main indicators: **the rate of persistent relative poverty and the sense of security in public space.** The indicator of consideration and cooperation and the indicator of contact between social groups will also be used either using the indicators to be developed within the scope of the Estonia 2035 strategy or by developing them ourselves.

**The rate of people living in persistent relative poverty** is the indicator in Estonia 2035 which indicates the share of people whose equivalent income was lower than the relative poverty threshold in the year under review and in at least two of the three preceding years. Persistent relative poverty reflects the division of income in society: the rate of relative poverty in society will not change if the income of people increases, but the division of income between them remains the same. The relative poverty rate of the citizens of Tallinn is significantly lower than the average in Estonia (16.7% in 2018) and at the same level as the EU average (12% in 2018)[^1]. The increase in the rate of persistent relative poverty reflects the tendency highlighted in the explanation of 'Integrated society', which has been observed in many European and US cities – the difference in wealth between various income groups often increases with the growth of wealth. The rate of relative poverty of children has grown significantly less in Tallinn during the same period (from 11.5 to 12.1) and the respective indicator in Estonia as a whole decreased from 17.3 to 15.2. In addition to the change in the rate of persistent relative poverty, the achievement of the target levels of the indicators established in the objectives and action programmes of social welfare must be observed.

Sense of security in city districts **(the share of residents who feel fully secure in the public places of their city district)** is measured with annual satisfaction surveys. The total share of residents who feel fully or rather safe in their districts has increased by 18 percent in comparison with 2012. The share of those who feel fully safe has increased even more (33%). An overview of the results of the 2018 survey by city district is given in the figure below.

![](../meedia/lisad/joonis-5_en.svg)

_Figure 1. How safe (%) do the residents of Tallinn feel in the public places of their city districts? (Satisfaction Survey of the Citizens of Tallinn 2018)_

As the share of people who feel fully or rather safe exceeds 90%, the share of people who feel fully safe should increase (without the total share of the sense of security decreasing).

**Changes in the average income of Tallinn subdistricts** are observed, as the goal 'Kind community' also addresses isolation. This indicates the extent to which the economic status of residents in city districts and subdistricts differs. In 2018, people in seven out of the eight districts of Tallinn earned more than the average income in Estonia (1234 euros). City Centre and Pirita stand out among the other districts of the city in terms of income (over 1600 euros), but the income of the residents of Lasnamäe is approximately 100 euros smaller the average in Estonia[^2].

Income inequality (income quintile share ration) is also an indicator of sustainable development. The quintile ratio has decreased in Estonia since 2000, dropping from 6.3 to 5.1 (2018). Estonia ranks 9th among European Union Member States with this. According to the average value of the indicators of European Union Member States weighted with the number of residents, the 20% of the population of Estonia with the highest income in 2017/2018 earned 5.1 times more than the 20% whose income was lowest. The given coefficient differed significantly between EU Member States and was 3.4 in Slovenia and the Czech Republic, more than 6.0 in Greece, Latvia, Romania and Spain, more than 7.0 in Lithuania and the largest, i.e. 8.2, in Bulgaria.[^3] In Tallinn, this indicator was equal to the Estonian average in 2018, i.e. 5.1.

The progress towards the strategic goal is also assessed on the basis of the indicators set out in other contributing areas. For example, the sense of security in public space can increase if the number of **traffic accidents with human victims** (the number in 2019 was 505), the number of **other accidents with human victims** (the health service providers in Tallinn registered 76,055 injuries in 2018) and **the number of offences** decrease. Security is also increased by the decrease in income inequality, which in turn is influenced by, for example, **the decrease of inequality in health** as well as **the indicators of the accessibility of primary medical care and specialised medical care.** There is no doubt that the decrease in income inequality is influenced by **all goals and action programmes in the area of education,** as the education system can be used to ensure that all people have equal opportunities for acquiring the good general skills required for work as well as top-level knowledge.

## City's areas of activity that support the achievement of this goal

The strategic goal 'Kind community' mainly addresses the relations between people, the security of the urban space and ensuring a dignified life and independent coping. **The areas of social welfare, healthcare and business environment** contribute the most to these topics.

Action programmes in **the area of social welfare** contribute to independent coping so that a dignified life and coping is guaranteed to everyone who needs help. For this purpose, welfare services are provided and social benefits are paid to children and families, disabled people, the elderly and other risk groups. The activities in the field prefer measures that help improve the ability of people to arrange their lives as independently as possible. Here, it is very important to assess the need for help in a timely and comprehensive manner and to organise the necessary assistance. The services provided for the prevention of social exclusion and promotion of independent coping also contribute to the improvement of the citizens' sense of security. In order to strengthen the sense of security, people who need help are provided with services aimed at guaranteeing that victims receive assistance that meets their needs as well as the opportunity to make a fresh start for a life free of violence. Services supporting families and safe house services are provided for this purpose.

**Business environment** – The existence of jobs (i.e. high employment) creates the preconditions for independent coping. All action programmes in the area of business environment aim to increase business activity, improve the international competitiveness of companies and create greater added value. This contributes to companies being more sustainable and earning bigger profits and to people earning higher salaries, which would improve society's standard of living. Adaptation of working environments so that people with special needs or elderly people can go to work improves the independent coping capabilities of the most vulnerable social groups.

**Health and healthcare** – The area contributes to an integrated society and independent coping as well as a strong sense of security. For this purpose, the accessibility of medical care to underprivileged people is ensured and the safe development of children and young people is supported.

In addition to the aforementioned areas, other aspects that contribute to the achievement of the strategic goal 'Kind community' can be mentioned in respect of the following areas.

**Municipal order** – The area primarily supports the achievement of the strategic goal by raising the awareness of citizens about how everyone can and must contribute to the creation of their own security and that of others. The city cooperates with the Police and Border Guard Board and the Rescue Board to ensure security. The priorities in the area of municipal order when guaranteeing security are traffic, public order and trade, which are included in all action plans of the area – prevention, presence and supervision.

**Culture** – The area and all of its action programmes encourage the people of Tallinn to be more caring, friendly and tolerant and contribute to the achievement of a more integrated society. The goal of the area is to ensure that the living environment in Tallinn fosters creativity and that there are excellent opportunities in the city for cultural activities, which increase contact between people. The developing and innovative urban space connects people with different cultural backgrounds; a person's interest in their own culture and other cultures is natural and fostered. Cultural events introducing the cultures of ethnic minorities are supported, which gives all citizens the chance to experience the traditions of ethnic minorities. Valuing the culture of different nationalities creates cohesion and stronger friendships between the different ethnic groups living in Tallinn. Cultural hobbies also bring together people of different socio-economic backgrounds, which creates cohesion. The activities of citizens' associations, which create a sense of locality by organising events and strengthen the sense of unity of communities, are also supported.

**Education and youth work** – The area of education ensures that all citizens have the opportunity to acquire good general skills and top-level knowledge, thereby contributing to independent and dignified coping and the integration of society. A suitable environment is created for this purpose and the model of an individual learning path is implemented, which supports the development of each learner. Suitable learning opportunities will also be created for all learners with special educational needs and the necessary support will also be ensured. Teachers use modern teaching methods and inspire students. Learning focuses on new and future-oriented skills. The development of learning opportunities in Estonian contributes to the integration of society. Tallinn schools provide the best education in Estonian and opportunities for learning in Estonian are also developed so that newcomers are also able to learn the language. The people who are temporarily in Estonia are also considered and offered the opportunity to acquire primary and general education in English. The activities of youth work are aimed at the inclusion of risk groups and increasing the entrepreneurship of young people.

**Sports and physical activity** – The independent coping of people largely depends on their health. The area contributes with its activities to the majority of citizens being active and engaging in sports throughout the year and thereby being in good health for a long time. Awareness of the usefulness of sports and physical activity will be increased for this purpose and opportunities for active mobility and exercise will be created for all citizens both in the immediate vicinity of their homes as well as in citywide sports facilities. Active mobility and exercise also foster contact between people, which in turn increase consideration and cohesion. All kinds of sporting events help bring people together and thereby create opportunities for increasing cohesion.

**Preservation and development of city property** – The area contributes to the integration of society by planning the real estate environment as well as social housing units and municipal housing in different regions of the city. Cooperation with the area of social affairs contributes to the provision of all people in need of help with housing that meets their needs and the requirements of legislation.

**Urban planning** – The action programmes of the area help design the kind of urban space that fosters the meetings of people and thereby increase cohesion. Busy streets strengthen the sense of security. Space which has been developed according to the principles of safety reduces the number of accidents.

[^1]: Eurostat. https://ec.europa.eu/eurostat/databrowser/view/tessi010/default/table?lang=en
[^2]: https://blog.stat.ee/2019/06/17/tallinna-rikkamad-ja-vaesemad-asumid/
[^3]: https://ec.europa.eu/eurostat/statistics-explained/images/5/5f/Inequality_of_income_distribution%2C_2017_%28Income_quintile_share_ratio%29.png

</Content>
