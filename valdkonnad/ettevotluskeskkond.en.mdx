---
slug: business-environment
title: Business environment
group: valdkond
background: "#0072CE"
---

<Hero>

<RunningHeader>Field of activity</RunningHeader>

## Business environment

**A city where the future is today.** Tallinn as the capital region is well known both in Estonia and abroad as a centre of smart enterprise, where new technology and business models and flexible forms of working are used. A flexible but stable legal and tax environment that makes it possible to implement new business models and an attractive living and operating environment promote the arrival of talent, investments and international companies to the city. Tallinn is an attractive tourist destination that exceeds the expectations of visitors. Tourism in Tallinn is sustainable: tourism does not damage the environment or the local historical and cultural heritage and creates value for the local community. Innovative, adaptable and skilled people are implemented in the creation of new value that helps increase the wellbeing of citizens. Companies focused on growth and foreign markets cooperate with research institutions in order to develop new products and services. The City of Tallinn is an internationally renowned, enthusiastic leader and tester of innovative solutions. International conferences help Tallinn to become more well known internationally as a competence centre in various fields.

</Hero>

<Content>

**This field contributes to a very large extent to the achievement of the strategic goal 'Creative global city' and to a large extent to the achievement of the strategic goals 'Kind community', 'Green transformation' and 'Friendly urban space'.**

**[Creative global city](/en/creative-global-city)** – Increasing interest and awareness in business as well as creating the image of an open city helps develop smart production and services of high added value. An attractive operating environment, international conferences and major events attract the best people to Tallinn and increase international awareness of Tallinn. All of this contributes to the development of business, research and innovation and allows Tallinn and Helsinki together to become the heart of smart economy in Northern Europe and an environment for testing innovative solutions.

**[Kind community](/en/kind-community)** – An entrepreneurial lifestyle, the promotion of business and the creation of sustainable jobs create better possibilities for coping independently and help create a strong sense of security.

**[Green transformation](/en/green-transformation)** – The use of innovative and smart solutions promotes eco-innovation by contributing to the mitigation of and adaptation to climate change and the creation of a good living environment. The balanced and sustainable development of tourism contributes to it having the smallest possible ecological footprint.

**[Friendly urban space](/en/creative-global-city)** – Tourists are also clients of companies in the service and creative sector. As a result, there are more unique retail and service companies, cultural events and entertainment opportunities for citizens and guests alike, which also enrich the urban space.

<!---
<ListHeader>UN sustainable development goals linked to this field</ListHeader>

<ImageGrid maxWidth="88px">

![YRO](../meedia/yro/goal_eng_01.png)

![YRO](../meedia/yro/goal_eng_02.png)

![YRO](../meedia/yro/goal_eng_03.png)

![YRO](../meedia/yro/goal_eng_04.png)

![YRO](../meedia/yro/goal_eng_05.png)

![YRO](../meedia/yro/goal_eng_06.png)

![YRO](../meedia/yro/goal_eng_07.png)

![YRO](../meedia/yro/goal_eng_08.png)

![YRO](../meedia/yro/goal_eng_09.png)

![YRO](../meedia/yro/goal_eng_10.png)

![YRO](../meedia/yro/goal_eng_11.png)

![YRO](../meedia/yro/goal_eng_12.png)

![YRO](../meedia/yro/goal_eng_13.png)

![YRO](../meedia/yro/goal_eng_14.png)

![YRO](../meedia/yro/goal_eng_15.png)

![YRO](../meedia/yro/goal_eng_16.png)

![YRO](../meedia/yro/goal_eng_17.png)

![YRO](../meedia/yro/goal_eng_18.png)

</ImageGrid>
--->

## Principles for implementation of the field

**Entrepreneurship, innovation and creativity** – Entrepreneurship, innovation and the commercialisation and testing of new technology and business models are supported. The creative potential of the residents of Tallinn makes it possible to offer distinctive and unique experiences. Innovative and smart solutions are used to increase the competitiveness of the destination, preserve the rich historical and cultural heritage of the city, inspire people to discover the city, disperse the burden of tourism and motivate guests to make environmentally friendly solutions.

**Ambition and internationality** – The development of companies is supported and their courage to export is increased.

**Responsibility and sustainability** – Environmentally friendly and responsible business is promoted. The development of tourism in Tallinn is balanced: the guests of the city gain pleasant experiences and the locals feel the beneficial impact of tourism.

**Physical environment that favours business** – Investments are made in a human-friendly urban space that supports the development of companies and promotes innovation.

**Cooperation** – Development and marketing activities are carried out in cooperation with the city, the state, entrepreneurs, research institutions and local communities.

## Goals of the field

1. **The residents of Tallinn are entrepreneurial and well-paid and local companies are ambitious.**

   _Encouraging citizens to be entrepreneurial and engage in innovative business ventures promotes the realisation of the growth potential of companies. Many new companies are established in Tallinn that are characterised by high added value and an increasing capacity to start using new technology and implement innovations. High added value enables companies to earn more profit, pay higher salaries and thereby improve the standard of living._

   <ListHeader>INDICATORS</ListHeader>

   - **Companies per 1000 residents (the share of companies and the contribution of the companies of global e-residents are also observed).**
     Starting level: 24 companies per 1000 residents (1 January 2019)  
     Target level: average annual growth rate of +2%
   - **Average gross salary in Tallinn**  
     Starting level: 1611 euros (Q4 2019, source: Statistics Estonia PA004: average gross salary, labour expenses and hours worked by county (quarters))  
     Target level: average annual growth rate of +3%

2. **Tallinn companies are competitive in global value chains when they work together.**

   The companies operating in the city are competitive both on domestic and foreign markets, including their export capacity will improve and foreign investments will increase. The focus is on knowledge-intensive economy, which is supported by digitalisation, good education and the good language skills of the people of Tallinn. Cooperation and development are cluster-based and include implementation of university expertise. Companies integrated into global value chains also attract national suppliers. The improvement of productivity has increased acquisitions of fixed assets and investments made by companies in people and R&D (intangible assets, e.g. acquisition of software, training of employees, development of the organisation and business processes, reputation building, design, R&D). The private sector, the public sector, the third sector and research institutions cooperate successfully.

   <ListHeader>INDICATORS</ListHeader>

   - **Share of export in turnover.**  
     Starting level: 3.4% (2019, source: Entrepreneurship Statistics of the Tax and Customs Board)
     Target level: average annual growth rate of +5%
   - **Companies with a global reach whose head office or development centre is in Tallinn and whose staff costs amount to 5 million euros per year.**  
     Starting level: 40 companies (2019, source: Entrepreneurship Statistics of the Tax and Customs Board)  
     Target level: on average +1 company per year

3. **Tallinn is also internationally known as an innovative environment.**

   The nature of work and people's expectations regarding work have changed considerably by 2035. Mobility, flexibility, self-realisation opportunities and social relationships are valued above a stable workplace. Tallinn takes these needs into consideration, the living and business environment in the city is competitive compared with other European countries and cities and attracts foreign workers who have the necessary skills, knowledge and experience to offer the city. Tallinn is well known as a breeding ground for start-ups and a meeting place for digital nomads.

   <ListHeader>INDICATORS</ListHeader>

   - **Number of residents working in start-ups.**  
     Starting level: 5200 (Q4 2019, source: Startup Estonia)  
     Target level: average annual growth rate of +7%
   - **Competitive position and success in international competitions.**  
     II–III place in the European Capital of Innovation contest (2017)  
     Target level: place among the top twenty digital and sustainable cities or in competitions (e.g. European Green Capital, European Digital City Index, CityKeys, Expat City Ranking)

4. **Tallinn is an internationally known and valued visitor destination all year round.**

   Tallinn attracts visitors as a city of exciting contrasts where historical and cultural heritage of various eras can be seen, and which has been given new life as a result of creativity and innovation. Tallinn is made attractive by the medieval Old Town, which is on the UNESCO World Heritage list, different neighbourhoods, clean and varied urban nature, diverse cultural events that take place all year round and local food. There is reason to visit Tallinn all year round. As a modern, environmentally conscious and innovative capital, Tallinn offers ideal opportunities for the organisation of conferences, motivation trips and events. Tallinn is an attractive filming location for foreign producers. Tallinn is hospitable.

   The number of foreign tourists staying overnight in Tallinn increases, especially during the low season, the destination countries of foreign tourists become more diverse, trips become longer, foreign visitors spend more money in Tallinn and the satisfaction of foreign visitors is high.

   <ListHeader>INDICATORS</ListHeader>

   - **Number of overnight stays by foreign tourists at accommodation establishments.**  
     Starting level: 2.8 mln (2019)
     Target level: 4.2 mln (2035)
   - **Number of overnight stays by Estonian residents at accommodation establishments.**  
     Starting level: 0.47 mln (2019)  
     Target level: 0.7 mln (2035)
   - **Number of foreign visitors who stay overnight.**  
     Starting level: 2.15 mln (2019)
     Target level: 3.1 mln (2035)
   - **Satisfaction of foreign visitors.**  
     Initial level: 8.8 out of 10 (2019)
     Target level: 9 out of 10 (2035)
   - **Export of travel services.**  
     Starting level: 1 bn euros (2019)  
     Target level: 2 bn euros (2035)
   - **Revenue per foreign visitor by target group.**  
     Starting level: the indicator will be developed in 2021  
     Target level: will be set after the starting level has been determined

## Action programmes

1. **Entrepreneurial lifestyle, entrepreneurs with ambitions of growth, top-level knowledge and skills**

   Support for entrepreneurship and cleverness starts via playful activities in kindergarten (robotics, study games) and continues at school. Independence and an entrepreneurial spirit, decision-making capacity and enthusiasm are fostered in children. Students in Tallinn schools are taught business at a high level. Students are enterprising and active in the establishment of student companies; one hundred student companies are established every year. Organisations that support entrepreneurship offer people interested in entrepreneurship and operating companies the opportunity to develop and update their business models, improve their management, sales and marketing skills and thereby increase their development capacity. Participation in entrepreneurship events is active (around 2500 participants per year). Approximately one hundred companies are established every year with the support of business consultants. Students participate in business idea development programmes at universities.

   **Key courses of action:** 1)&nbsp;increasing interest in entrepreneurship (in cooperation with the Tallinn Education Department, Tallinn Business Village, educational programme 'Entrepreneurial School', the Junior Achievement entrepreneurship education programme with a fair of student companies); 2)&nbsp;providing business advisory services and improving the knowledge and skills of entrepreneurs; 3)&nbsp;supporting contests of new business ideas and the (pre-)incubation programmes of universities ('Edu ja tegu' programme) and business incubators (creative incubator, Start-up Incubator); 4)&nbsp;supporting the capability and growth of companies for operation on the local market and entering foreign markets, implementing digital solutions, developing products/services of higher added value and promoting cooperation with professional institutions and higher education institutions; and 5)&nbsp;strengthening the initiatives of organisations that support entrepreneurship by supporting non-profit activities.

2. **An environment open to internationalisation and cooperation between sectoral stakeholders**

   Tallinn has the image of an innovative city that welcomes talent. There are good opportunities and connections in the city for facilitating the mobility of talent and the spread of knowledge. The twin city of Tallinn and Helsinki has developed into the heart of smart economy in Northern Europe, where workforce moves flexibly in both directions. All of the information and services foreign nationals and their employers need for living and working in Estonia has been made accessible. Tallinn is in the top 20 of the Expat City Ranking.

   **Key courses of action:** 1)&nbsp;services for foreign specialists and new immigrants (recruitment and adaptation programme of the International House); 2)&nbsp;creation of synergy between innovation centres (Ülemiste City, Tallinn Science Park Tehnopol, research institutions); 3)&nbsp;establishment of connections (Rail Baltic terminal, tram connections with city gates); 4)&nbsp;international marketing activities aimed at increasing awareness of Tallinn; and 5) stimulation of the development of entrepreneurship clusters and international networking (including creative and circular economy) and promotion of cooperation between the parties to the ecosystem of innovation.

3. **Smart city programme**

   The international ambition of Tallinn is to become a leading centre of smart cities and innovation among the smaller capital cities of the world. Another objective of the city is to set an example for other cities in the Baltic Sea region in the digital development of local authorities. To this end, the city deals centrally with the management and empowerment of innovation, including enhancing competencies and supporting innovative solutions.

   **Key courses of action:** 1)&nbsp;creation of data-based digital city governance; 2)&nbsp;increasing the proportion of urban digital services in public services and updating them; 3)&nbsp;increasing the role of the city as a local authority in ordering and initiating innovation via innovative public procurements, public service design, demo projects, etc.; 4)&nbsp;fostering the development of new solutions of future technology and their (test) implementation in the city environment (e.g. pilot projects of self-driving cars, over-the-air free Internet, smart solutions for the residents and guests of Tallinn); and 5)&nbsp;implementation of the joint projects of Tallinn and Helsinki.

4. **Attractive physical environment for the development of entrepreneurship**

   Good connections and a quality public space have been developed in larger business and industrial parks and business incubators provide quality services for the development of new ambitious business models and sustainable development to companies. Tallinn is one of the owners of the Tallinn Business Incubators Foundation, Tallinn Science Park Tehnopol and Tallinna Tööstusparkide AS (industrial parks). They are attractive operating locations of high added value for technology, production and creative companies. The city earns income by favouring the business environment as well as the companies in which the city has a holding.

   **Key courses of action:** 1)&nbsp;development of business incubators; 2)&nbsp;development of associations of business and industrial parks _(implemented with action programmes 1 and 2 in mobility, city planning will also contribute_; 3)&nbsp;development of the Tallinn Science Park Tehnopol as a smart city testing environment; and 4) organisation of business development projects that support entrepreneurship, are profitable and generate added value for the city (e.g. Tallinn Film Wonderland, new areas of the technology business).

5. **Recognised tourism destination and balanced development of tourism**

   Tallinn is an internationally well-known and highly valued destination of recreational and business tourism in the Baltic Sea region that attracts tourists all year round. Awareness of the destination is increased by means of international communication and marketing on strategically important target markets and to important target groups.

   Smart solutions help show and experience the rich historical and cultural heritage of Tallinn to the maximum. Tourist information is accessible and personalised.

   The development and marketing of tourism is based on data, analyses and monitoring. Local tourism companies are environmentally conscious, familiar with the principles of universal design, aware of responsible and sustainable business and apply this knowledge in the development of tourism services. The hospitable destination is safe and accessible to everyone. The burden of tourist visits is dispersed. The people of Tallinn are hospitable, the increase in the number of tourists will not reduce their satisfaction. A memorable visit inspires people to return to Tallinn and recommend it to their friends.

   Various sectors of the city organisation contribute to the implementation of the action programme: culture, sports and physical activity; mobility; city planning and environmental protection. The state and the private sector also have a very important role.

   **Key courses of action:** 1)&nbsp;maintaining the positive image of a tourism destination and increasing international awareness of Tallinn as a destination for city breaks any time of the year; 2)&nbsp;increasing awareness of Tallinn as a destination of conference tourism and motivational trips; 3)&nbsp;organising data-based and balanced development of tourism; 4)&nbsp;offering memorable and convenient city visits with the Tallinn Card; and 5)&nbsp;guaranteeing the accessibility of quality tourist information services.

</Content>
