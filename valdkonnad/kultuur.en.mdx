---
slug: culture
title: Culture
group: valdkond
background: "#0072CE"
---

<Hero>

<RunningHeader>Field of activity</RunningHeader>

## Culture

**City air promotes creativity.** Culture is the creator of the wellbeing of citizens and the guide for the city's development. The cultural space of Tallinn promotes creativity, people here value innovation and cooperation, the atmosphere is inspiring and free of prejudice. Competent specialists work in the field of culture; there is a constant dialogue between local and international creators; the opinions of creators of culture and creative entrepreneurs are taken into consideration in many fields (city planning, education and youth work, social welfare and health and healthcare).

An open and innovative city connects different values, movements and forms of expression. Every citizen has been given opportunities for self-realisation and creative activities, communities actively promote the cultural life of their neighbourhoods, Tallinn as the capital of Estonia guarantees the environment and tools for the development of the field by increasing the visibility of the city and the state in international competition.

Tallinn values its rich historical and cultural heritage, both material and spiritual: The Old Town, historical buildings, monuments and parks are in good order, works of art and museum exhibits are attractively presented in contemporary memory institutions, the tradition of the Song and Dance Festival offers joy and a great experience to new generations of dancers and singers as well as to the guests of the celebrations.

</Hero>

<Content>

**This field contributes to a very large extent to the achievement of the strategic goal 'Creative global city' and to a large extent to the achievement of the strategic goals 'Kind community' and 'Friendly urban space'.**

**[Creative global city](/en/creative-global-city)** – The versatile cultural landscape contributes to the development of people's creativity and invites tourists and talent to Tallinn.

**[Kind community](/en/creative-global-city)** – Good opportunities for activities increase contact between people and increase the integration of society. Valuing the cultures of different nationalities creates cohesion between the ethnic groups living in Tallinn.

**[Friendly urban space](/en/creative-global-city)** – Cultural institutions and events make the urban space more interesting. They create reasons for being in the city; therefore, there are more people in the urban space, which in turn makes the city more attractive.

<!---
<ListHeader>UN sustainable development goals linked to this field</ListHeader>

<ImageGrid maxWidth="88px">

![YRO](../meedia/yro/goal_eng_01.png)

![YRO](../meedia/yro/goal_eng_02.png)

![YRO](../meedia/yro/goal_eng_03.png)

![YRO](../meedia/yro/goal_eng_04.png)

![YRO](../meedia/yro/goal_eng_05.png)

![YRO](../meedia/yro/goal_eng_06.png)

![YRO](../meedia/yro/goal_eng_07.png)

![YRO](../meedia/yro/goal_eng_08.png)

![YRO](../meedia/yro/goal_eng_09.png)

![YRO](../meedia/yro/goal_eng_10.png)

![YRO](../meedia/yro/goal_eng_11.png)

![YRO](../meedia/yro/goal_eng_12.png)

![YRO](../meedia/yro/goal_eng_13.png)

![YRO](../meedia/yro/goal_eng_14.png)

![YRO](../meedia/yro/goal_eng_15.png)

![YRO](../meedia/yro/goal_eng_16.png)

![YRO](../meedia/yro/goal_eng_17.png)

![YRO](../meedia/yro/goal_eng_18.png)

</ImageGrid>
--->

## Principles for implementation of the field

**Equal opportunities regardless of the social, age or cultural characteristics of people** – Cultural activities are easy to access.

**Different ways of engaging in culture are valued** – Professional higher education, folk culture and recreational activities are supported.

**The development of digital culture is supported** – Digitalised cultural heritage will be made accessible to the next generations and new, digitally created, distributed and consumed content is supported.

**Local and international creators of culture are involved** – Cooperation with national, private and third-sector creators of culture is pursued in the development of the field of culture. Professional organisers of culture are used for the implementation of events initiated to mark the topics and historical events important for the city.

**Responsible organisation of culture** – Cultural events are organised in an environmentally conscious and economically and socially sustainable manner.

## Goals of the field

1. **All people in Tallinn can develop their creativity.**

   Creativity and cooperation are an important resource for problem solving and being successful in 21st-century society. In order to ensure the development of Tallinn as a creative and global city, the conditions necessary for the cultural self-realisation of citizens are taken into consideration when planning the public space. Creators of culture and their traditional and new initiatives in various fields of culture are valued and highlighted in communication and shape the reputation of Tallinn at the local and international level. Tallinn is a highly valued working and living environment among creative people.

   <ListHeader>INDICATORS</ListHeader>

   - **Employment in the arts, entertainment and recreation sector in Tallinn.**  
     Starting level: 7800 (Tallinn in numbers 2020)  
     Target level: average annual growth rate of +1%

2. **The cultural events and leisure facilities in Tallinn are attractive and accessible to the citizens and visitors.**

   Professional organisation of culture and active citizens' initiatives enrich the city's cultural calendar and make Tallinn an attractive living environment. Opportunities for quality recreation have been created in the public spaces near people's homes and in cultural institutions. Cultural events are accessible and meet the expectations of the residents. The satisfaction with leisure opportunities for the residents and guests of Tallinn will increase.

   <ListHeader>INDICATORS</ListHeader>

   - **Satisfaction with cultural leisure opportunities in Tallinn.**  
     Starting level: will be set in 2021 (Tallinn satisfaction survey)  
     Target level: will be set after the starting level has been determined

3. **Urban space is the second living room.**

   The public space in Tallinn – streets, squares, beaches and green areas – is like a second living room for citizens, where conditions have been created for cultural life and various activities that can be pursued every day and all year round. Infrastructure and communications have been smartly organised in the centres and meeting places of different regions, which makes it possible to organise both traditional and experimental public events securely and flexibly. An information technology application is used, which allows street architects to register their public performances in a convenient and transparent manner. It is pleasant and safe to move around in Tallinn. The prerequisite for attaining this goal is that the proposals of creators of culture and the needs of the local community are considered in the development visions of the city and in the planning of its infrastructure.

   <ListHeader>INDICATORS</ListHeader>

   - **Satisfaction with cultural leisure opportunities in urban space near home.**  
     Starting level: will be set in 2021 (satisfaction survey)  
     Target level: will be set after the starting level has been determined
   - **The other objectives of the sector have been met if the target levels of indicators of the third action programme have been met.**

## Action programmes

1. **Rich and accessible cultural calendar**

   Many professionally organised cultural events take place in Tallinn, the cultural calendar offers something for everyone all year round irrespective of the person's age, gender, nationality, interests or social background. Many international festivals take place in Tallinn and important contemporary cultural, memory and recreational institutions are located here. The sea is one of the dominants in Tallinn – the seaside is part of the active urban space and an inspiring environment rich in opportunities for organisers of culture. Visiting cultural events is easy for everyone and the cultural infrastructure is accessible. The cultural services provided by the city consider the situation on the cultural landscape and meet the expectations of the residents. Tallinn is a popular tourist destination, and the city's tourist attractions and cultural calendar offer experiences both for domestic and foreign tourists.

   **Key courses of action:** 1)&nbsp;supporting the organisation of cultural events and projects taking place in Tallinn; 2)&nbsp;supporting and developing important cultural sites and top-level cultural events; 3)&nbsp;cooperation at the international level, 4)&nbsp;improvement of the accessibility of information on cultural events; 5)&nbsp;improvement of the accessibility of culture to school-age children; and 6)&nbsp;integration of cultural services, cooperation with the state and private sectors and citizens' associations.

2. **Accessible memory and lifelong learning**

   In the 21<sup>st</sup> century, creativity – the creation of new connections – is a mainstay that distinguishes wealthy and happy societies from poor and stagnant ones. Memory institutions are no longer passive collectors of information but provide opportunities for experiencing our cultural heritage and create preconditions for constant innovation. Museums, libraries, the zoo and botanical gardens, the folk university and other course organisers are contemporary infrastructures of lifelong learning, which offer residents and guests various opportunities to discover, learn, enjoy and create. The number of visits to institutions offering memory and cultural education and people's satisfaction with them increases.

   **Key courses of action:** 1)&nbsp;development of the network of libraries and museums; 2)&nbsp;collection, preservation and research of cultural heritage; 3)&nbsp;digitisation of cultural heritage and making it accessible in new channels; and 4)&nbsp;modernisation of services, design of new services, cooperation in international projects.

3. **Diverse opportunities for cultural activities**

   World-class works of art are created, and professional cultural services are offered in Tallinn. In addition to professional creators of culture, whose self-realisation and entrepreneurship is supported by the city in every way possible, there are hobby centres in Tallinn where people have plenty of opportunity to practice their hobbies. User-friendly solutions for the cross-usage of premises meant for hobby activities have been created in the infrastructure belonging to the city, including school buildings. Possibilities have been created for giving new life to empty buildings and plots – the urban space and infrastructure are in the service of active and smart citizens. Cultural hobbies develop people's creativity, are a part of lifelong learning and preserve the habit of learning. Common interests also bring together groups of different nationalities and social and economic backgrounds, helping to establish stronger friendships and cohesion.

   **Key courses of action:** 1)&nbsp;creation of diverse opportunities for hobbies and recreational activities; 2)&nbsp;improvement of the network of cultural centres and updating of services; 3)&nbsp;planning public urban space suitable for cultural activities; 4)&nbsp;international cooperation, participation in foreign projects; and 5)&nbsp;cooperation with citizens' associations and delegation of services.

4. **City of Song and Dance Festivals**

   The tradition of song and dance festivals, which belongs on the UNESCO Intangible Cultural Heritage List, is an inseparable part of the national identity of Estonians. This big event needs separate attention and planning due to the long preparation process.

   The nationwide song and dance festivals are grand achievements of national culture and will be organised in Tallinn in a hundred years' time as well. The infrastructure required for the organisation of the celebrations has been well developed and meets the needs of the performers and spectators. Choral singing, folk dancing and playing musical instruments are still popular among the people of Tallinn and people's desire to participate in the celebrations is big. The instructors of the collectives participating in the preparation process are highly motivated and valued in society.

   **Key courses of action:** 1)&nbsp;supporting the collectives participating in the Song and Dance Festival; and 2)&nbsp;systematic and equal remuneration of the supervisors of school collectives participating in the Song and Dance Festival.

5. **Culturally integrated Tallinn**

   Culture brings people of different backgrounds together. It is good to live, develop, create and promote culture in Tallinn for both Estonians and representatives of the other nationalities living here. The cultural institutions of Tallinn support the joint activities of people with different language and cultural backgrounds, creation and participation in culture, introduce the cultural activities of ethnic minorities to the public, consider the needs of new immigrants and help them adapt seamlessly to the local conditions and integrate into the society and the culture.

   Cultural events of ethnic minorities, which are highly appreciated by the audiences and bring people together, are held in Tallinn. These events allow people to perceive Tallinn as a multinational global city. Tallinn has broad horizons, is open and interested in the world and the relationships between communities are based on support and trust.

   **Key courses of action:** 1)&nbsp;supporting the cultural events that introduce the cultures of ethnic minorities; 2)&nbsp;supporting the activities of the cultural associations of ethnic minorities; 3)&nbsp;supporting the activities of the Russian Cultural Centre and the International Union of National Cultural Associations Lyre; and 4)&nbsp;development of services supporting the adaptation of new immigrants, improvement of outreach, cooperation with the state and third sector.

6. **Citizens' associations add colour to neighbourhoods**

   Tallinn is made diverse by its compact (regional) centres with quality space and numerous functions, which are attractive and offer communication and activity opportunities all year round. Active subdistrict groups and citizens' associations operate in different regions, which organise interesting events for the residents of their neighbourhoods and guests: subdistrict festivals, fairs, sports competitions, family events, etc. The city government cooperates closely with local entrepreneurs and residents. Subdistrict groups are organisers of cultural life who contribute to the development of their neighbourhoods and the city. They create a sense of place, strengthen the feeling of unity and increase the value of the region.

   **Key courses of action:** 1)&nbsp;cooperation with citizens' associations and foundations, supporting and introducing their activities; and 2)&nbsp;supporting the events organised by citizens' associations and foundations.

</Content>
