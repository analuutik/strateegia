---
slug: health-and-healthcare
title: Health and healthcare
group: valdkond
background: "#0072CE"
---

<Hero>

<RunningHeader>Field of activity</RunningHeader>

## Health and healthcare

**Tallinners are healthy and vivacious.**

Tallinners who are in good health can realise themselves in an integrated, safe, biodiverse and healthy city. Tallinn is a city where the risks arising from the living environment and people's health behaviour are minimal.

</Hero>

<Content>

**This field contributes to a very large extent to the achievement of the strategic goal 'Healthy mobility' and to a large extent to the achievement of the strategic goals 'Creative global city' and 'Kind community'.**

**[Healthy mobility](/en/healthy-mobility)**, **[Creative global city](/en/creative-global-city)** and **[Kind community](/en/kind-community)** – Promoting a healthy lifestyle and ensuring the accessibility of medical care increase the number of healthy life years, help people cope independently, increase the sense of security and increase employment.

<!---
<ListHeader>UN sustainable development goals linked to this field</ListHeader>

<ImageGrid maxWidth="88px">

![YRO](../meedia/yro/goal_eng_01.png)

![YRO](../meedia/yro/goal_eng_02.png)

![YRO](../meedia/yro/goal_eng_03.png)

![YRO](../meedia/yro/goal_eng_04.png)

![YRO](../meedia/yro/goal_eng_05.png)

![YRO](../meedia/yro/goal_eng_06.png)

![YRO](../meedia/yro/goal_eng_07.png)

![YRO](../meedia/yro/goal_eng_08.png)

![YRO](../meedia/yro/goal_eng_09.png)

![YRO](../meedia/yro/goal_eng_10.png)

![YRO](../meedia/yro/goal_eng_11.png)

![YRO](../meedia/yro/goal_eng_12.png)

![YRO](../meedia/yro/goal_eng_13.png)

![YRO](../meedia/yro/goal_eng_14.png)

![YRO](../meedia/yro/goal_eng_15.png)

![YRO](../meedia/yro/goal_eng_16.png)

![YRO](../meedia/yro/goal_eng_17.png)

![YRO](../meedia/yro/goal_eng_18.png)

</ImageGrid>
--->

## Principles for implementation of the field

**Health in every field.** Health-related factors and measures largely belong in the area of responsibility of social, economic, environmental or other fields. Therefore, it is important that the impact on health is taken into consideration in the development and implementation of all action plans. Health systems ensure a comprehensive approach to services, covering health promotion, disease prevention, integrated treatment and organisation of service provision in cooperation with the health and social system as well as the coordination of service providers, institutions and systems, irrespective of whether this falls under the public or private sector. It is also important that the healthcare services provided in detention houses and detention centres comply with the requirements established in the health system.

**Reduction of inequality.**. Differences in gender, age, region or other characteristics can often be seen in the case of indicators that characterise health. Based on health indicators, help must be given to those who need it most and they should be regarded as the target group when activities are planned. This reduces health-related inequality.

**Cooperation.** Cooperation between the state, the third sector and the private sector must be made more efficient in order to achieve the health objectives. It is important to ensure that cooperation covers different fields by involving communities at the national and local level. Cooperation with people is aimed at developing suitable solutions for health preservation and treatment as well as for coping with diseases. The community and the local level have a very important role in improving people's living environment and supporting and promoting health.

## Goals of the field

1. **The physical activity of citizens has increased, their diet has become more balanced and risk behaviour has decreased.**

   Every day, people make decisions and choices that may have a good or bad impact on their health. Decision-making depends on the economic, political, cultural and socio-psychological impact of the living environment, which shapes the values, opportunities and lifestyles of people. The people of Tallinn make increasingly more decisions that are informed and promote positive health behaviour.

   Optimal physical activity, a balanced diet, safe road use and leisure activities, risk-free sexual behaviour and lack of addictive habits help prevent many diseases and allow people to be as healthy as possible. As a result of <Annotation id="health-literacy">health literacy</Annotation>, the health of the citizens of Tallinn is better, their health awareness improves, they use fewer health services and time spent in hospitals becomes shorter, which in turn reduces the costs of healthcare.

   In a society where alcohol policy is based on the interests of public health and agreements, good conditions have been created for the leisure activities of young people and fitness, where a healthy lifestyle and family relationships are supported with political decisions, solidarity prevails and people care about one another; the health behaviour of people is also balanced and proceeds from values. The people of Tallinn live increasingly longer, and the number of healthy life years is also going up.

   <ListHeader>INDICATORS</ListHeader>

   - The target levels of the indicators of the action programmes for the development of health behaviour of citizens (AP1, AP3, AP4) have been achieved.

2. **Primary care and specialised medical care are guaranteed to all Tallinn citizens when necessary.**

   All people have the same rights and possibilities to use health services irrespective of their age, gender, place of residence, special needs or social background. Tallinn contributes to the development and provision of health services in order to improve the health of its citizens. A population group or person may be preferred in the case of limited resources only if necessary because of their health needs. People not covered by health insurance receive primary medical care in Tallinn and inpatient follow-up care is guaranteed to them. Healthcare is focused on people, patients get enough knowledge and support in decision-making and inclusion in the treatment process and their next of kin are able to help them in a supportive environment. The population's awareness of the risks arising from the misuse of medicinal products, especially antibiotics, is improving. Health services are accessible and of high quality thanks to the development of integrated systems. Health literacy and the ability to cope with illness improves. Satisfaction with health services grows, the job satisfaction of healthcare professionals and the efficiency of the system improves and the increase in healthcare expenses is under control. Taking responsibility for patient safety is the main duty and ethical obligation of every healthcare professional. The sustainability of the healthcare system is ensured as obligations are divided fairly between the state, local authorities, healthcare service providers, employers and people. The Tallinn Hospital planned in cooperation between the state and the city ensures the accessibility of quality specialised medical care to the population and the improvement of the quality of treatment.

   <ListHeader>INDICATORS</ListHeader>

   - **Share of people who are very satisfied and satisfied with access to healthcare services.**  
     Starting level: 95.6% (2018)  
     Target level: 98% (2035)
   - **Share of pupils covered by the health checks of Tallinn School Health among pupils referred for scheduled checks.**  
     Starting level: 91.6% (2019)  
     Target level: 95% (2035)
   - **Share of uninsured persons in Tallinn receiving first contact care and inpatient follow-up care services among uninsured persons.**  
     Starting level: 2.2% (2019)  
     Target level: 1.5% (2035)

## Action programmes

1. **Ensuring the health and safe development of children and young people**

   The better the child's development and living conditions and the more supportive and caring the relationships in their family, the better the child will cope economically and socially in their adulthood. The way families with children are supported is therefore extremely important from the viewpoint of public health. The experience gained during childhood influences the development of the future adult's values, social coping skills and health behaviour. Children from families that cope socially and economically have more opportunities to participate in age-appropriate and developing activities and to acquire education that corresponds to their abilities. Children are healthier and happier as a result of better social coping and health behaviour, there are fewer mental and behavioural disorders, the learning outcomes of children have improved, and they manage better in society. The mortality of infants and children under the age of five decreases. The age of first-time users of tobacco and tobacco-like products, alcohol and drugs increases.

   **Key courses of action:** 1)&nbsp;supporting the health and wellbeing of pregnant women, babies and small children; 2)&nbsp;prevention of injuries sustained by children at home and during leisure time; 3)&nbsp;coordination of efficient and systematic information exchange between parents, educational institutions and first contact healthcare providers; 4)&nbsp;development of a sustainable system of services and programmes supporting positive parenting; 5)&nbsp;development of a sustainable counselling system for families of children with growing, educational and behavioural difficulties; 6)&nbsp;early detection and prevention of non-infectious diseases and their risk factors; 7)&nbsp;reduction of the accessibility of tobacco products and alcohol to minors; 8)&nbsp;reduction of drug use and increase in the age of first-time users; and 9)&nbsp;reduction of risky sexual behaviour and the spread of HIV and guaranteeing professional and consistent counselling.

2. **Healthcare services at the best contemporary level are accessible to everyone who needs help**

   Healthcare services are provided in cooperation with the state, the private sector and research institutions. Primary care services are provided by family doctors and the healthcare professionals working with them. Health centres improve the accessibility and quality of help. Primary care services are provided in contemporary health centres in all regions of the city. It is possible to get appointments with medical specialists within a reasonable time. Health centres and hospitals are easy to access. Quality medical care in a good physical environment is accessible to everyone.

   The establishment of the Tallinn Hospital, which will offer health services at a high level, is being planned. Competent specialists will be working in the hospital, modern medical equipment will be procured, and the hospital's physical environment is patient friendly.

   **Key courses of action:** 1)&nbsp;guaranteeing the accessibility of primary healthcare services, including to persons not covered by health insurance; 2)&nbsp;expanding the possibilities of day care and support services; 3)&nbsp;inclusion of citizens in defining local health needs and risks; 4)&nbsp;realisation of the Tallinn Hospital project; 5)&nbsp;guaranteeing 24-hour emergency medical care (Tallinn Ambulance); and 6)&nbsp;development of the network of Tallinn medical institutions in cooperation with the state, the private sector and research institutions for the provision of high-quality and accessible services.

3. **Living, working and learning environment that supports health**

   The impact of the development and implementation of the policies of all areas of activity of the city on people's health is considered. In the living environment, attention is primarily given to the cleanliness of water and air, adherence to noise limits and prevention of accidents. In the working and learning environment, it is most important to ensure a healthy indoor climate and reduce <Annotation id="psychosocial-risk-factors">psychosocial risk factors</Annotation>. This action programme is implemented by city agencies that are responsible for the preservation, development and design of the physical environment as well as for providing environmental education.

4. **Healthier lifestyle, healthy choices**

   Optimal physical activity, a balanced diet, safe road use and leisure activities, risk-free sexual behaviour and lack of addictive habits help prevent many diseases and allow people to be as healthy as possible for their entire lives. The spread of HIV in Tallinn is slowing down. The satisfaction of the citizens with their health improves. The share of people with balanced eating habits increases and the share of overweight and obese people decreases in all age groups. The number of daily smokers (including consumers of snus and e-cigarettes) decreases and the use of drugs in all age groups goes down. The awareness of young people and adults of safe sexual behaviour improves. The incidence of cardiovascular diseases, malignant tumours and chronic non-infectious diseases decreases.

   **Key courses of action:** 1)&nbsp;increasing the physical activity of the population, including children and young people (instruction and coordination of physical activity, mobility and education); 2)&nbsp;raising the health awareness of the city residents; 3)&nbsp;reducing the accessibility of tobacco products and alcohol; 4)&nbsp;improving the accessibility of high-quality treatment and rehabilitation services and <Annotation id="harm-reduction-service">harm reduction services</Annotation> to drug addicts and HIV-positive people; 5)&nbsp;promotion of the security-conscious behaviour of the residents of Tallinn in traffic, in daily life and when spending leisure time; and 6)&nbsp;enhancement, development and empowerment of location-based health promotion.

</Content>
