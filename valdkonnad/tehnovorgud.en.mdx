---
slug: utility-networks
title: Utility networks
group: valdkond
background: "#0072CE"
---

<Hero>

<RunningHeader>Field of activity</RunningHeader>

## Utility networks

Tallinn has contemporary, quality and maintained utility networks. This makes it possible to create a modern urban space and ensures the accessibility of services whose environmental impact is as small as possible.

</Hero>

<Content>

**This field contributes to a very large extent to the achievement of the strategic goal 'Home that includes the street' and to a large extent to the achievement of the strategic goals 'Friendly urban space', 'Healthy mobility', 'Green transformation' and 'Creative global city'.**

**[Home that includes the street](/en/home-that-includes-the-street)** – Reliable and modern utility networks help ensure the comfort and energy efficiency of homes. Public sewerage and water supply help reduce household costs and protect the environment.

**[Friendly urban space](/en/friendly-urban-space)**, **[Healthy mobility](/en/healthy-mobility)**, **[Creative global city](/en/creative-global-city)** – Planning compact routes for utility networks makes it possible to establish greenery and above-ground infrastructure and design an attractive urban space. Well planned lighting helps establish a safe and pleasant urban space.

**[Green transformation](/en/friendly-urban-space)** – Environmentally friendly and cost-effective solutions and minimisation of environmental damage upon the development of utility networks make it possible to save energy and protect the natural environment.

<!---
<ListHeader>UN sustainable development goals linked to this field</ListHeader>

<ImageGrid maxWidth="88px">

![YRO](../meedia/yro/goal_eng_01.png)

![YRO](../meedia/yro/goal_eng_02.png)

![YRO](../meedia/yro/goal_eng_03.png)

![YRO](../meedia/yro/goal_eng_04.png)

![YRO](../meedia/yro/goal_eng_05.png)

![YRO](../meedia/yro/goal_eng_06.png)

![YRO](../meedia/yro/goal_eng_07.png)

![YRO](../meedia/yro/goal_eng_08.png)

![YRO](../meedia/yro/goal_eng_09.png)

![YRO](../meedia/yro/goal_eng_10.png)

![YRO](../meedia/yro/goal_eng_11.png)

![YRO](../meedia/yro/goal_eng_12.png)

![YRO](../meedia/yro/goal_eng_13.png)

![YRO](../meedia/yro/goal_eng_14.png)

![YRO](../meedia/yro/goal_eng_15.png)

![YRO](../meedia/yro/goal_eng_16.png)

![YRO](../meedia/yro/goal_eng_17.png)

![YRO](../meedia/yro/goal_eng_18.png)

</ImageGrid>
--->

## Principles for implementation of the field

The interests of citizens as well as cost-efficient and environmentally friendly technical solutions are primarily followed when the utility networks of Tallinn are planned. The utility networks running on the same route are built simultaneously in order to avoid excessive excavation and minimise environmental damage. The routes of utility networks are planned in a compact manner on an area of land that is as small as possible so that green areas and aboveground structures can be built. The construction and reconstruction of utility networks is planned, if possible, with the construction and reconstruction of the other infrastructure of the city.

The city cooperates with nearby municipalities in the development of public water supply and sewerage because Tallinn belongs to the same Harju sub-basin as them. It is ensured that no threat of pollution is created as a result of the activities of all parties.

Only separated sewerage systems (wastewater, rainwater) are built, and local dispersion (impregnation) and natural treatment (planned ponds, marshes, etc.) of rainwater is incorporated as recommended by the Helsinki Committee (HELCOM).

Provision of safe wastewater in the amount of 25 l/d per person and wastewater drainage is ensured via the public water supply in <Annotation id="emergencies">emergencies</Annotation>. Supplying consumers with heating via the district heating network in emergencies is also guaranteed, and possibilities for connecting network regions are guaranteed and container boiler houses are used for this purpose. Heat supply may be interrupted for a maximum of 24 hours and the service is primarily guaranteed to hospitals and social, housing and educational institutions.

## Goals of the field

1. Utility networks are contemporary, of high quality and maintained.

   <ListHeader>INDICATORS</ListHeader>

   - The target levels of all indicators of the action programmes have been achieved.

## Action programmes

1. **Water supply**

   The infrastructure is modern and maintained. Breakdowns in water supply (e.g. water accidents) are minimal, water loss is less than 15%. The quality of the water service is high, and it is accessible to the citizens. It is possible for all registered lots located in areas covered by the public water supply system of Tallinn to connect to the public water supply. Stable water supply is guaranteed in emergencies and the accessibility of drinking water to residents is ensured. Separate connection points for all registered lots have been established during the reconstruction of water pipelines that run through several registered lots.

   Drinking water is clean and safe, i.e. it meets the requirements set forth in national legislation and Council Directive 98/83/EC. The price of the service is affordable to all residents. The developments taking place in neighbouring municipalities are taken into consideration when the activities required for guaranteeing the quality of the water of Lake Ülemiste are planned.

   **Key courses of action:** 1)&nbsp;reconstruction of water pipelines and thereby reducing breakdowns and water losses and increasing the reliability of the network; 2)&nbsp;promotion of connection to public water supply; 3)&nbsp;analysing the possibility of building a water supply system based on groundwater that covers the entire city and is intended for emergencies; 4)&nbsp;constant modernisation of the Ülemiste water treatment plant, sustainable production of drinking water with the smallest possible environmental impact; 5)&nbsp;reconstruction of the hydraulic structures of the Tallinn surface water intake; 6)&nbsp;reconstruction of groundwater intakes and treatment systems; 7)&nbsp;ensuring the supply of fire extinguishing water from public water supply in areas where this is technically possible and expedient; 8)&nbsp;ensuring the accessibility of drinking water in public space; and 9)&nbsp;promoting the sustainable use of water.

2. **Wastewater systems**

   The sewerage infrastructure is modern and maintained. The number of breakdowns in the provision of sewerage services (e.g. clogging) is minimal. High-quality and seamless sewerage services are provided to all residents of Tallinn. It is possible to get connected to the public sewerage system in the area covered by the public sewerage system of Tallinn and on the registered immovables located in the wastewater collection area. Connection points have been or will be established on registered immovables in such a manner that the installation of pipes on third registered lots is avoided, if possible.

   Wastewater discharged into receiving bodies of water stations meets the requirements provided for in the Minister of the Environment Regulation and in the European Urban Waste Water Treatment Directive No. 91/271/EC. The needs of industries are taken into consideration when wastewater treatment capacities are planned and the dimensions of the main structures of the system are determined accordingly.

   **Key courses of action:** 1)&nbsp;reconstruction of pipelines; 2)&nbsp;promotion of connection to the public sewerage system; 3)&nbsp;changing the sewerage system from a combined to a separate sewerage system; 4)&nbsp;proper wastewater treatment; 5)&nbsp;increasing the treatment capacity; 6)&nbsp;modernisation and improvement of wastewater treatment technology; 7)&nbsp;inspection of local wastewater treatment and supporting the proper construction of facilities; and 8)&nbsp;updating the public water supply and sewerage development plan of Tallinn.

3. **Rainwater drainage**

   The system for the discharge of surface water, rainwater and drainage water has been reconstructed and built and is contemporary and economical. Increasingly more registered lots are connected to the rainwater sewerage system, and floods and waterlogging in residential areas have been minimised.

   The share of the separate rainwater sewerage system has increased, which makes it possible to prevent an increase in the load of the Paljassaare wastewater treatment plant and the discharge of contaminated rainwater into the sea. Local impregnation and delay measures are applied upon drainage of rainwater in new development areas. Rainwater is preferably impregnated or treated as a natural resource that is accumulated and used in a reasonable manner, e.g. in toilets and for watering.

   The following is guaranteed upon stormwater treatment:

   - Good ecological status of rainwater recipients – coastal sea and bodies of water in the city. Constant improvement of the quality of rainwater directed to the recipient.
   - Collection and use of rainwater in a reasonable manner – primarily based on its generation.
   - Reduction of the flow rates of discharged rainwater and the pollution transmitted to the environment by stormwater.
   - Turning combined sewerage systems into separate ones.
   - Prevention of floods and mitigation of their consequences. Mapping of areas at risk of floods and minimising the negative impact caused by floods.

   **Key courses of action:** 1)&nbsp;development of a rainwater policy; 2)&nbsp;implementation of solutions of sustainable and weatherproof rainwater systems (e.g. local impregnation, extension of the residing time of rainwater and more extensive use of it); 3)&nbsp;rainwater treatment; 4)&nbsp;construction and reconstruction of a separate rainwater sewerage system; 5)&nbsp;regular cleaning of streets, use of environment-friendly snow repellents to guarantee the cleanliness of rainwater and surface water; 6) liquidation of illegal wastewater connections to rainwater systems; 7)&nbsp;maintenance and regular construction of ditches/streams/culverts; 8)&nbsp;updating the source documents (standards) of the rainwater system design; and 9)&nbsp;analysis of the implementation of the remuneration system that promotes the handling of rainwater based on its emergence.

4. **Energy systems**

   The energy system is based on renovated and efficient district heating, gas and power networks and is further developing towards more dispersed energy production and ensuring higher security of supply. The thermal energy used in the district heating system is fully generated from renewable fuels. Systems for the production, distribution and accumulation of power and thermal energy based on green technology are implemented in open market and open competition conditions in regions of new developments and small residential buildings. Solar energy, (green) energy obtained at the energy exchange (especially night-time power), biofuels or biomethane are used as primary energy and hydrogen as secondary energy.

   The heating network is technically in order and heat loss has been reduced to a minimum. The share of pre-insulated district heating pipelines has increased. The number of regions included in the district heating network has increased and the use of district heating has increased. All district heating networks are efficient according to the Energy Efficiency Directive (2012/27/EU) and use renewable fuels at base load. District cooling has been implemented in shopping and business centres, office buildings and new developments of apartment buildings where necessary and where practical.

   Approximately 50% of the power needed in the city will be produced in Tallinn in 2030. The city creates the preconditions for supporting the community energy associations that produce energy from solar panels and, for example, store hydrogen. The buildings belonging to the city have been transferred to the 380 V voltage system. Readiness for transition to the 380 V voltage system is guaranteed in the entire city and the share of the registered lots connected to the new voltage system has increased. Losses in power distribution grids are minimal. High voltage overhead lines have been replaced with underground cables.

   **Key courses of action:** 1)&nbsp;expansion of district heating regions, renovation of district heating networks and replacement of pipelines with pre-insulated pipes; 2)&nbsp;implementation of district cooling pilot projects and establishment of district cooling networks; 3)&nbsp;renovation and further development of power distribution networks; 4)&nbsp;extension of combined heat and power generation; 5)&nbsp;improvement of energy efficiency and increasing the share of renewable energy in production and consumption; 6)&nbsp;raising awareness of and capacity for the use of solar energy in local power supply and diversification of renewable energy sources; and 7)&nbsp;use of waste unsuitable for recovery in energy production.

5. **Street lighting**

   Contemporary energy-efficient LED lighting has been implemented in the city and lighting-based control is functioning on main roads. The city's total power consumption per lighting point has decreased. Special lighting is used at pedestrian crossings and special solutions have been created for lighting parks and other public areas. People feel safer in the urban space at night, as its well lit. Light pollution is minimal.

   **Key courses of action:** 1)&nbsp;ensuring the functioning of lighting systems; 2)&nbsp;replacing all lighting fixtures with LED technology-based lighting fixtures; 3)&nbsp;introduction of fixture-based control; 4)&nbsp;establishment of special lighting at pedestrian crossings; 5)&nbsp;establishment of lighting solutions for the creation of a safe and cosy urban space; and 6)&nbsp;replacement of lines with open wires with cable lines.

</Content>
