---
slug: environmental-protection
title: Environmental protection
group: valdkond
background: "#0072CE"
---

<Hero>

<RunningHeader>Field of activity</RunningHeader>

## Environmental protection

Tallinn is a city of environmentally aware people that offers many natural benefits.

The environment is valued in Tallinn; sustainable use of the environment prevails. Tallinn's residents value the diverse natural environment, understand the links between nature and human activity, and behave responsibly. People know that natural resources must be used sustainably, and the good condition of the environment must be preserved. The living environment in Tallinn is pleasant. People of all ages can acquire good environmental education in the contemporary environmental education centres and the purposeful financing of environmental education and its expedient development are ensured.

</Hero>

<Content>

**This field contributes to a very large extent to the achievement of the strategic goal 'Green transformation' and to a large extent to the achievement of the strategic goals 'Friendly urban space', 'Home that includes the street', 'Healthy mobility' and 'Creative global city'.**

**[Green transformation](/en/green-transformation)** – Collection and management of information about the natural environment and the development of guidelines help plan the preservation of biodiversity and diverse urban nature and mitigate climate change. Improving environmental awareness inspires people to behave sustainably. Waste management contributes to resource-efficient living.

**[Friendly urban space](/en/friendly-urban-space)** and **[Home that includes the street](/en/home-that-includes-the-street)** – Cleaner air, cleaner bodies of water and less noise make streets and neighbourhoods attractive. The increase in environmental awareness also inspires citizens to make the urban space and the neighbourhoods more attractive.

**[Healthy mobility](/en/healthy-mobility)** – Clean air and water and noise reduction have a good impact on the mental and physical health of citizens. Species-rich and accessible green areas encourage people to be outdoors.

**[Creative global city](/en/creative-global-city)** – Diverse urban nature and a clean environment make Tallinn attractive to tourists, citizens and investors.

<!---
<ListHeader>UN sustainable development goals linked to this field</ListHeader>

<ImageGrid maxWidth="88px">

![YRO](../meedia/yro/goal_eng_01.png)

![YRO](../meedia/yro/goal_eng_02.png)

![YRO](../meedia/yro/goal_eng_03.png)

![YRO](../meedia/yro/goal_eng_04.png)

![YRO](../meedia/yro/goal_eng_05.png)

![YRO](../meedia/yro/goal_eng_06.png)

![YRO](../meedia/yro/goal_eng_07.png)

![YRO](../meedia/yro/goal_eng_08.png)

![YRO](../meedia/yro/goal_eng_09.png)

![YRO](../meedia/yro/goal_eng_10.png)

![YRO](../meedia/yro/goal_eng_11.png)

![YRO](../meedia/yro/goal_eng_12.png)

![YRO](../meedia/yro/goal_eng_13.png)

![YRO](../meedia/yro/goal_eng_14.png)

![YRO](../meedia/yro/goal_eng_15.png)

![YRO](../meedia/yro/goal_eng_16.png)

![YRO](../meedia/yro/goal_eng_17.png)

![YRO](../meedia/yro/goal_eng_18.png)

</ImageGrid>
--->

## Principles for implementation of the field

**Natural benefits ensure quality of life.** Everyone wants to live in a biodiverse environment surrounded by greenery. An urban environment like this offers natural benefits that improve the quality of people's lives in the areas surrounding their homes as well as in public urban space. Quality street greenery as well as land- and water-based ecosystems reduce air pollution and the problems caused by rainwater and also dampen noise, which is why the pressure on nature must be reduced and the quality of water and air improved.

**Common species should remain common.** It is important to ensure that the plant and animal species citizens encounter every day, as well as the rare species that have found habitats in the green areas of Tallinn, remain in good condition.

** "If you don't know, you don't respect; if you don't respect, you don't care or love" (Fred Jüssi).** It is important to raise the awareness of citizens in making decisions and choices that concern the surrounding environment. Citizens must therefore be given more information about nature and the natural values of Tallinn so that people can experience and see them with their own eyes.

**Waste hierarchy and waste prevention principle.** The generation of waste must be prevented first and foremost. Reuse of waste must be preferred in waste management. If this is not possible, waste must be recycled. Other reuse must be reduced, and it is advisable to avoid elimination of waste, including depositing.

## Goals of the field

1. **Nature in Tallinn is diverse, and the urban environment is healthy.**

   A diverse and good urban environment provides natural benefits (food, drinking water, pollination, flood barrier, climate control, etc.), which ensure the good quality of life and satisfaction of citizens – this distinguishes Tallinn from many other capitals. Without leaving the city, residents and visitors of Tallinn can spend their free time by the sea, lakes and rivers and visit old forests, diverse meadows and mire communities.

   <ListHeader>INDICATORS</ListHeader>

   - The target levels of all indicators of the action programmes have been achieved.

2. **Waste management is sustainable.**

   The environmental risk arising from waste generation and waste has decreased, waste is recycled to the maximum or reused in other ways. Waste management in Tallinn is at a better level than elsewhere in Estonia.

   <ListHeader>INDICATORS</ListHeader>

   - **Percentage of municipal waste growth in the percentage of gross domestic product (GDP) growth.**  
     Starting level: under ½ of GDP growth percentage (2015)  
     Target level: under ½ of GDP growth percentage (annually)
   - **Share of municipal waste recycling.**  
     Starting level: 50% (2020)
     Target level: over 65% (2030)

## Action programmes

1. **Diverse and rich urban nature**

   Tallinn has a good overview of biodiversity, the status of habitats and the activities required for maintaining biodiversity and natural benefits, including as a result of the impact of climate change. Habitats are connected to one another and the most valuable areas have been taken under protection. The status of indicator species is regularly monitored, and the common species citizens encounter almost every day that make them happy have spread all over the city. The protected areas of Tallinn and all other green areas are properly maintained. The area of protected areas has increased. Biotic communities are taken into consideration when daily maintenance, design and planning decisions are made.

   The action programme of the smart lifecycle of greenery in the area of urban landscapes contributes directly to the implementation of the action programme.

   **Key courses of action:** 1)&nbsp;inventory of habitats; 2)&nbsp;organisation of the conservation of protected areas and individual objects; 3)&nbsp;improvement of the status of species and habitats; 4)&nbsp;monitoring the status of protected and common species; and 5)&nbsp;considering the principles of protection and compensation of habitats upon planning and designing.

2. **Clean water**

   The impact of climate change on the condition of surface and groundwater, water consumption, the threat of flooding and the increase in coastal erosion of the coast will be taken into consideration. Tallinn has a good overview of the quality of surface water and groundwater and the activities necessary for maintaining and improving their status. Tallinn has a good overview of the risks of flooding and coastal erosion and the activities required to avoid and reduce them. The ecological, physical and chemical status of the coastal waters and freshwater bodies of Tallinn has improved due to more efficient surveillance, cleaner rainwater and reduction of the pollution load. The majority of groundwater resources are protected, and the quality of reserves is guaranteed. The quality of bathing water meets requirements during the entire bathing season. The release of pathogens, nutrients and hazardous substances into water systems that use surface water as drinking water is prevented. The cleanliness of rainwater discharged into receiving bodies of water is ensured so that it does not deteriorate the ecological status of the water bodies.

   The area of utility networks (via the action programmes of water supply, sewage systems and rainwater sewerage) and the area of mobility (through street cleaning) contribute directly to the implementation of the action programme.

   **Key courses of action:** 1)&nbsp;monitoring and assessment of the status of bodies of water; 2)&nbsp;monitoring of the quality of wastewater; 3)&nbsp;improvement of the status of bodies of water; and 4)&nbsp;improvement of surveillance in order to end the unauthorised discharge of wastewater into bodies of water.

3. **Clean air**

   The quality of ambient air in Tallinn has improved and this is also positively reflected in the health of citizens. The measures taken in the residential areas where particular matter (PM) pollution is the worst have improved air quality. Citizens know more about the generation and reduction of air pollution (transport, heating, bonfires). The level of ambient air pollution corresponds to the established limits and target values and a radon-safe living environment is ensured for residents of Tallinn indoors.

   The programme is implemented first and foremost by the areas of mobility (through reducing transport emissions and cleaning streets), utility networks (through the expansion of district heating) and urban planning (establishment of environmental conditions for detailed plans where the risk of radon is taken into consideration).

   **Key courses of action:** 1)&nbsp;constant monitoring of the pollutants spreading in ambient air and performance of surveys.

4. **Less noise**

   The number of residents who suffer from noise limit exceedings has decreased, as the measures taken have been efficient. Tallinn has determined quiet areas that are protected from noise and has improved the residents' access to these areas. The city has prepared a strategic noise chart and an action plan for noise reduction that will be implemented as a result of projects and plans.

   The action programme is primarily implemented by three areas: mobility, urban planning and city property. The area of mobility takes care of increasing the share of bicycle traffic, redirecting heavy-duty vehicles away from the city centre and reducing the speed limit, calms traffic in residential areas and reduces the share of passenger cars in the city centre. The area of urban planning deals with the separation of noise-resistant areas (commercial and industrial land) from noise-sensitive areas (residential and social land). The task of the area of city property is to build more noise-resistant new buildings, protect existing buildings against noise and improve the sound insulation of new and existing apartment buildings next to main roads.

   **Key courses of action:** 1)&nbsp;updating and implementing the strategic noise chart and action plan; and 2)&nbsp;improving the accessibility of quiet areas.

5. **Waste management**

   Waste generation has decreased, recovery is promoted, and a consumer-friendly infrastructure has been created for this purpose. Prevention and reduction of waste generation depends mainly on people's awareness and consumer habits. Both city structures as well as private persons and companies contribute to the prevention of waste generation. The city can contribute to the prevention and reduction of waste generation by creating better possibilities for returning reusable items, organising outreach campaigns and implementing environmental management systems. All city structures have been transferred to the paper-free organisation of work. The main waste generated in households can be disposed of nearby. Each city district has a waste station of adequate size with a reuse centre, and construction waste is recovered as much as possible. Collection of waste by type is guaranteed everywhere, which reduces the share of deposited municipal waste. Waste transport functions efficiently everywhere in the city. Diversification of the possibilities for the collection of waste by type and the optimisation of the waste transport and handling network will minimise the share of waste exiting the circular economy. The Tallinn Waste Plan specifies the indicators, target levels and activity trends for the waste management objective.

   **Key courses of action:** 1)&nbsp;efficient operation of organised waste transport; 2)&nbsp;expansion of networks for collection of municipal waste; 3)&nbsp;release of substantial quantities of waste collected by type into circulation; 4)&nbsp;finding uses for the land of former landfills; 5)&nbsp;outreach concerning waste; and 6) improvement of the efficiency of city supervision over waste holders and waste handlers.

6. **Environmentally aware citizens**

   The environmental awareness of the residents of the Tallinn region has improved. Environmentally aware people make their decisions in consideration of good practices, scientifically proven positions and relevant environmental information. Risk groups are aware of the health risks caused by climate change. Citizens receive up-to-date information about environmentally friendly behaviour online and through campaigns. The network of environmental education centres has been developed with Tallinn Zoo, Kadriorg Park and the Tallinn Botanical Gardens. Environmental education is provided at all levels of education and in adult education. Residents are involved in the promotion of environmental education and in making environmental decisions. A network of community, training and rental gardens has been created in the city.

   The objectives of the action programme are implemented, among others, by the field of education (inspiring and innovative learning, individual learning path, a balanced and diverse education network, modern and developing youth work).

   **Key courses of action:** 1)&nbsp;creating possibilities for learning about nature and for environmental education in city institutions (including establishment of training gardens); 2)&nbsp;improving opportunities for environmental education in informal education and in-service training outside city institutions (including establishment of community gardens); 3)&nbsp;informing residents about current environmental issues (including impact of climate change); 4)&nbsp;supporting the initiatives of citizens that raise environmental awareness; 5)&nbsp;planning the location and development of new environmental education centres; and 6) development of innovative solutions to raise environmental awareness, including implementation of technological solutions.

</Content>
