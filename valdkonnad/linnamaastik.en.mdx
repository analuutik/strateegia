---
slug: urban-landscape
title: Urban landscape
group: valdkond
background: "#0072CE"
---

<Hero>

<RunningHeader>Field of activity</RunningHeader>

## Urban landscape

Tallinn has a valuable and aesthetic living environment with a functioning green network and a diverse landscape that has been designed and maintained in an environmentally sustainable manner.

</Hero>

<Content>

**This field contributes to a very large extent to the achievement of the strategic goals 'Friendly urban space', 'Home that includes the street' and 'Green transformation' and to a large extent to the achievement of the strategic goal 'Healthy mobility'.**

**[Friendly urban space](/en/friendly-urban-space)** – Maintained and diverse greenery makes green areas accessible and attractive and the street space pleasant.

**[Home that includes the street](/en/home-that-includes-the-street)** – Subdistricts and yards are unique, clean and safe. Yards are greener because of greenery and there are more possibilities for spending leisure time. By planning, building and maintaining playgrounds, the city offers children and families more opportunities and reasons to spend time in their neighbourhoods.

**[Green transformation](/en/green-transformation)** – Diverse green areas contribute to the preservation of biodiversity and adaptation to climate change.

**[Healthy mobility](/en/healthy-mobility)** – Diverse greenery and playgrounds help create an attractive urban space that promotes spending time and moving outdoors.

<!---
<ListHeader>UN sustainable development goals linked to this field</ListHeader>

<ImageGrid maxWidth="88px">

![YRO](../meedia/yro/goal_eng_01.png)

![YRO](../meedia/yro/goal_eng_02.png)

![YRO](../meedia/yro/goal_eng_03.png)

![YRO](../meedia/yro/goal_eng_04.png)

![YRO](../meedia/yro/goal_eng_05.png)

![YRO](../meedia/yro/goal_eng_06.png)

![YRO](../meedia/yro/goal_eng_07.png)

![YRO](../meedia/yro/goal_eng_08.png)

![YRO](../meedia/yro/goal_eng_09.png)

![YRO](../meedia/yro/goal_eng_10.png)

![YRO](../meedia/yro/goal_eng_11.png)

![YRO](../meedia/yro/goal_eng_12.png)

![YRO](../meedia/yro/goal_eng_13.png)

![YRO](../meedia/yro/goal_eng_14.png)

![YRO](../meedia/yro/goal_eng_15.png)

![YRO](../meedia/yro/goal_eng_16.png)

![YRO](../meedia/yro/goal_eng_17.png)

![YRO](../meedia/yro/goal_eng_18.png)

</ImageGrid>
--->

## Principles for implementation of the field

Greenery is an equal element alongside the artificial elements of the city environment.

The city proceeds from the integrity of the life cycle of greenery – planning, design, construction and maintenance are mindfully managed.

High-level landscape architecture is important – the existing values are preserved and brought to the foreground, and high-quality artificial elements with good designs that help value the landscape are added.

## Goals of the field

1. **The city is clean, green and attractive.**

   Tallinn has a network of green areas that ensures the preservation of different types of ecosystems and landscapes and balances the impacts of the population and economic activities. The landscape is diverse with bogs, lakes, rivers, cliffs and forests. The urban space is clean thanks to an efficient waste collection system and the decrease in waste generation. High-quality greenery, including greenery resistant to climate change, in green areas as well as in the street space increases people's mental and physical wellbeing and strengthens identity and satisfaction with the city. Proper maintenance of vegetation and compliance with the requirements for pet keeping helps ensure the safety of urban space.

   <ListHeader>INDICATORS</ListHeader>

   - **Satisfaction of citizens (the proportion of citizens who are very satisfied) with the cleanliness of the city.**  
     Starting level: will be set in 2021 (source: satisfaction survey of Tallinn)  
     Target level: will be set after the starting level has been determined
   - **The green areas belonging to the city are properly maintained.**  
     Starting level: 54% of the greenery belonging to the city is appropriately maintained (based on the requirements for the maintenance of green areas in Tallinn) (2020)  
     Target level: 100% (2035)

## Action programmes

1. **Smart life cycle of green areas**

   Green areas have been preserved in the city, which cover the needs of the citizens and ensure the preservation of flora and fauna. The <Annotation id="green-factor">green factor</Annotation> concept and nature-based and innovative solutions are implemented in planning and design, which help preserve and improve the quality of greenery and cope with climate change. Naturally regulating solutions are preferred when greenery is designed as opposed to spatial solutions that require a lot of maintenance. Green areas are designed in a manner that preserves and highlights the existing values and fits into the surrounding environment. Green areas are easily accessible, and their functionality is based on the needs of the citizens. Most city residents are satisfied with the condition of green areas. Innovative methods are used in designing and maintaining the green areas of the city. Plant species resistant to climate change are used and the diversity of species is increased in the green areas of the city. Green areas are constantly renewed so that they look good, serve their purpose and preserve the uniqueness of the locality. All green areas that require maintenance are appropriately maintained and most of the trees have passports. The number of trees will not decrease due to construction activity.

   **Key courses of action:** 1)&nbsp;planned management of green areas; 2)&nbsp;collection and management of data on green areas and recording them in the information system; 3)&nbsp;establishment, reconstruction and maintenance of green areas; 4) development of plans, guidance materials and principles, development of the best skills; and 5)&nbsp;raising awareness.

2. **Cemeteries and funeral services**

   Cemeteries are perceived as cemetery parks and parts of the green network. The cultural and natural values in cemeteries are exhibited better. Cemeteries kept in good order all year round, the organisation of funerals is ensured, and the cemetery database is constantly updated.

   **Key courses of action:** 1)&nbsp;cemetery services, including organisation of funerals; 2)&nbsp;cemetery maintenance; 3)&nbsp;maintenance of the cemetery database; 4)&nbsp;specific funeral services; and 5)&nbsp;shaping cemeteries into cemetery parks.

3. **Children's playgrounds**

   A network of playgrounds has been developed in Tallinn for children of different ages and with special needs and an action plan has been prepared for the establishment of playgrounds. Modern, diverse and safe playgrounds offer activities for all children – this way, children can spend more time outdoors and develop themselves. Children can play together and involve parents in their games as well. Most city residents are satisfied with playgrounds. Playgrounds are safe and well maintained and no accidents occur due to lack of maintenance.

   **Key courses of action:** 1)&nbsp;planning a network of playgrounds (including preparation of a programme); 2)&nbsp;construction of playgrounds; and 3)&nbsp;reconstruction and maintenance.

4. **Animal protection**

   People are aware of the responsibility associated with keeping pets. In order to ensure a friendlier living environment for pets and a cleaner and stress-free city for all residents, the city has built dog walking parks where owners can spend time with their pets. The equipment in walking parks offers activities for pets. Most citizens are satisfied with the location and number of walking parks. There are also some places where dogs are allowed to swim. 99% of dogs and cats in the city are microchipped and there are no strays in the city that would be a danger to themselves and to people. Most pets that end up in the shelter are returned to the owners or new owners are found for them.

   **Key courses of action:** 1)&nbsp;outreach concerning the protection of animals; 2)&nbsp;keeping the register of pets; 3) catching and keeping stray dogs and cats, returning them to the owners or finding new owners for them; 4)&nbsp;ensuring the welfare of animals (dog walking and swimming areas); and 5)&nbsp;preventive work in the field of animal protection.

5. **Public order**

   In addition to the main action programmes of the sector, projects are implemented that help make the urban environment more attractive. These projects may be permanent, seasonal or one-off. The use of the city environment is increased by well-selected and well-arranged city furniture, which diversifies the use of public space and thereby supports a healthy lifestyle and communication between people.

   Urban furniture is part of high-quality urban space. Urban furniture is functional (signage, benches, bins) and beautifully designed, enriches the urban space with carefully considered design, details and high-quality materials and highlights beautiful views.

   **Key courses of action:** 1)&nbsp;organisation of the property maintenance month; 2)&nbsp;installation of portable toilets; 3)&nbsp;construction of mobile ice fields; and 4)&nbsp;maintenance of beaches.

</Content>
